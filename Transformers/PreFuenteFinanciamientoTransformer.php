<?php
/**
 * Created by PhpStorm.
 * User: crisf
 * Date: 13/03/2019
 * Time: 11:09 PM.
 */

namespace Modules\Presupuesto\Transformers;

class PreFuenteFinanciamientoTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'Codigo'      => $item->codigo,
            'Nombre'      => $item->nombre,
            'Descripcion' => config('erp.lista_si_no.'.$item->descripcion),
            'Activo'      => config('erp.lista_si_no.'.$item->activo),
        ];

        return $output;
    }
}
