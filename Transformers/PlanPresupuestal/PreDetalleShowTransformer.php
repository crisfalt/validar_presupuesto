<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 3/04/2019
 * Time: 11:57 AM.
 */

namespace Modules\Presupuesto\Transformers\PlanPresupuestal;

use Illuminate\Database\Eloquent\Model;
use Themsaid\Transformers\AbstractTransformer;

class PreDetalleShowTransformer extends AbstractTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'id'                          => $item->id,
            'proyecto'                    => $item->proyecto,
            'valor'                       => $item->valor,
            'saldo'                       => $item->saldo,
            'descripcion'                 => $item->descripcion,
            'codigo_con_numero_documento' => $item->codigo_con_numero_documento,
            'pre_plan_presupuestal_id'    => $item->pre_plan_presupuestal_id,
            'tipo_operacion'              => $item->tipo_operacion,
            'documento_referencia'        => [
                'id'                => $item->encabezado_documento_referencia->id,
                'codigo_con_numero' => $item->encabezado_documento_referencia->codigo_con_numero_documento,
            ],
            'fuente_financiamiento' => ($item->fuente_financiamiento) ? [
                'id'     => $item->fuente_financiamiento->id,
                'codigo' => $item->fuente_financiamiento->codigo,
                'nombre' => $item->fuente_financiamiento->nombre,
            ] : null,
            'centro_costo' => [
                'id'     => $item->centro_costo->id,
                'nombre' => $item->centro_costo->nombre,
            ],
            'rubro' => [
                'id'                => $item->plan_presupuestal->id,
                'codigo_con_nombre' => $item->plan_presupuestal->codigo_con_nombre,
                'codigo'            => $item->plan_presupuestal->codigo_rubro,
                'plan_inicial'      => $item->plan_presupuestal->plan_inicial,
                'saldo'             => $item->plan_presupuestal->saldo,
            ],
            'tercero' => ($item->tercero) ? [
                'id'                          => $item->tercero->id,
                'codigo_con_numero_documento' => $item->tercero->numero_documento_con_nombre_completo,
            ] : null,
            'proyecto' => ($item->proyecto) ? [
                'id'     => $item->proyecto->id,
                'nombre' => $item->proyecto->nombre,
            ] : null,
            'detalles_hijos' => [
                (!$item->detalles_hijos->isEmpty()) ? self::transform($item->detalles_hijos) : null,
            ],
        ];

        return $output;
    }
}
