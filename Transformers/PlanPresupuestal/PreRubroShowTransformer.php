<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 1/04/2019
 * Time: 6:00 PM.
 */

namespace Modules\Presupuesto\Transformers\PlanPresupuestal;

use Illuminate\Database\Eloquent\Model;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Themsaid\Transformers\AbstractTransformer;

class PreRubroShowTransformer extends AbstractTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'id'                       => $item->id,
            'pre_plan_presupuestal_id' => $item->pre_plan_presupuestal_id,
            'com_vigencia_id'          => $item->com_vigencia_id,
            'com_cgr_id'               => $item->com_cgr_id,
            'tipo_rubro_id'            => $item->tipo_rubro_id,
            'codigo'                   => $item->codigo_rubro,
            'nombre_rubro'             => $item->nombre_rubro,
            'codigo_con_nombre'        => $item->codigo_con_nombre,
            'maneja_movimiento'        => $item->maneja_movimiento,
            'plan_inicial'             => $item->plan_inicial,
            'adicciones'               => $item->adicciones,
            'reducciones'              => $item->reducciones,
            'creditos'                 => $item->creditos,
            'contra_creditos'          => $item->contra_creditos,
            'activo'                   => $item->activo,
            'disponible'               => PrePlanPresupuestalService::saldoDisponible($item),
        ];

        return $output;
    }
}
