<?php

namespace Modules\Presupuesto\Transformers\Impresiones;

use Illuminate\Database\Eloquent\Model;
use Themsaid\Transformers\AbstractTransformer;

class PreImpresionDatatableTransformer extends AbstractTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'codigo_con_numero_documento_referencia' => $item->codigo_con_numero_documento,
            'fecha_documento'                        => $item->fecha_documento,
            'descripcion'                            => $item->descripcion,
            'total'                                  => $item->total,
            'estado'                                 => array_search($item->estado, (array) config('erp.estado_documento')),
        ];
        switch (config('erp.maestros_documentos.'.@$this->options[0].'.prefijo')) {
            case config('erp.maestros_documentos.scdp.prefijo'):
                $output['responsable'] = $item->tercero1->numero_documento_con_nombre_completo;
                break;
            case config('erp.maestros_documentos.ascdp.prefijo'):
                $output['responsable'] = $item->tercero1->numero_documento_con_nombre_completo;
                break;
            case config('erp.maestros_documentos.cdp.prefijo'):

                break;
            case config('erp.maestros_documentos.com.prefijo'):
                $output['tercero'] = $item->tercero1->numero_documento_con_nombre_completo;
                $output['documento_referencia'] = $item->documento_referencia->codigo_con_numero_documento;
                $output['usuario'] = propietarioDocumento($item)->numero_documento_con_nombre_completo;
                break;
            case config('erp.maestros_documentos.rp.prefijo'):
                $output['tercero'] = $item->tercero1->numero_documento_con_nombre_completo;
                $output['documento_referencia'] = $item->documento_referencia->codigo_con_numero_documento;
                $output['usuario'] = propietarioDocumento($item)->numero_documento_con_nombre_completo;
                break;
            case config('erp.maestros_documentos.gp.prefijo'):
                $output['tercero'] = $item->tercero1->numero_documento_con_nombre_completo;
                $output['documento_referencia'] = $item->documento_referencia->codigo_con_numero_documento;
                $output['usuario'] = propietarioDocumento($item)->numero_documento_con_nombre_completo;
                break;
        }

        return $output;
    }
}
