<?php

namespace Modules\Presupuesto\Transformers\Impresiones;

use Illuminate\Database\Eloquent\Model;
use NumerosEnLetras;
use Themsaid\Transformers\AbstractTransformer;

class PreImpresionDocumentoTransformer extends AbstractTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'encabezado_documento' => [
                'id'                          => $item->id,
                'prefijo'                     => $item->prefijo,
                'codigo_con_numero_documento' => $item->codigo_con_numero_documento,
                'numero_documento'            => $item->numero_documento,
                'fecha_documento'             => $item->fecha_documento,
                'vigencia_presupuestal'       => $item->vigencia_presupuestal->nombre,
                'descripcion'                 => $item->descripcion,
                'estado'                      => array_search($item->prefijo, (array) config('erp.estado_documento')),
                'fecha_confirmacion'          => $item->fecha_confirmacion,
                'fecha_aplazamiento'          => $item->fecha1,
                'copias'                      => $item->copias,
                'subtotal'                    => $item->subtotal,
                'total'                       => $item->total,
                'total_letras'                => NumerosEnLetras::convertir($item->total, 'Pesos', false, 'Centavos'),
                'empresa'                     => [
                    'id'     => $item->empresa->id,
                    'nombre' => $item->empresa->nombre,
                ],
                'sede' => [
                    'id'        => $item->sede->id,
                    'nombre'    => $item->sede->nombre,
                    'telefono'  => $item->sede->telefono,
                    'direccion' => $item->sede->direccion,
                ],
                'documento' => [
                    'id'     => $item->documento->id,
                    'codigo' => $item->documento->codigo,
                    'nombre' => $item->documento->nombre,
                ],
                'centro_costo' => ($item->centro_costo1) ? [
                    'id'     => $item->centro_costo1->id,
                    'nombre' => $item->centro_costo1->nombre,
                ] : 'Sin especificar',
                'documento_referencia' => ($item->encabezado_documento_referencia) ? [
                    'id'                => $item->encabezado_documento_referencia->id,
                    'codigo_con_numero' => $item->encabezado_documento_referencia->codigo_con_numero_documento,
                ] : 'Sin especificar',
                'tercero' => ($item->tercero1) ? [
                    'id'                                   => $item->tercero1->id,
                    'razon_social'                         => $item->tercero1->razon_social,
                    'nombre1'                              => $item->tercero1->nombre1,
                    'nombre2'                              => $item->tercero1->nombre2,
                    'apellido1'                            => $item->tercero1->apellido1,
                    'apellido2'                            => $item->tercero1->apellido2,
                    'numero_documento_con_nombre_completo' => $item->tercero1->numero_documento_con_nombre_completo,
                    'nombre_representante_legal'           => $item->tercero1->nombre_representante_legal,
                ] : 'Sin especificar',
                'propietario' => [
                    'id'                                   => propietarioDocumento($item)->id,
                    'numero_documento_con_nombre_completo' => propietarioDocumento($item)->numero_documento_con_nombre_completo,
                ],
            ],
            'detalles_presupuestales' => (!$item->detalles_presupuestales->isEmpty()) ? PreImpresionDetalleTransformer::transform($item->detalles_presupuestales) : null,
            'codigo_QR'               => [
                'empresa_id'   => $item->empresa->id,
                'sede_id'      => $item->sede->id,
                'documento_id' => $item->id,
            ],
        ];

        return $output;
    }
}
