<?php

namespace Modules\Presupuesto\Transformers\Impresiones;

use Illuminate\Database\Eloquent\Model;
use Themsaid\Transformers\AbstractTransformer;

class PreImpresionDetalleTransformer extends AbstractTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'id'                       => $item->id,
            'valor'                    => $item->valor,
            'pre_plan_presupuestal_id' => $item->pre_plan_presupuestal_id,
            'documento_referencia'     => [
                'id'                => $item->encabezado_documento_referencia->id,
                'codigo_con_numero' => $item->encabezado_documento_referencia->codigo_con_numero_documento,
            ],
            'fuente_financiamiento' => ($item->fuente_financiamiento) ? [
                'id'     => $item->fuente_financiamiento->id,
                'codigo' => $item->fuente_financiamiento->codigo,
                'nombre' => $item->fuente_financiamiento->nombre,
            ] : null,
            'tercero' => ($item->tercero) ? [
                'id'                         => $item->tercero->id,
                'razon_social'               => $item->tercero->razon_social,
                'nombre1'                    => $item->tercero->nombre1,
                'nombre2'                    => $item->tercero->nombre2,
                'apellido1'                  => $item->tercero->apellido1,
                'apellido2'                  => $item->tercero->apellido2,
                'nombre_representante_legal' => $item->tercero->nombre_representante_legal,
            ] : null,
            'centro_costo' => [
                'id'     => $item->centro_costo->id,
                'nombre' => $item->centro_costo->nombre,
            ],
            'proyecto' => ($item->proyecto) ? [
                'id'     => $item->proyecto->id,
                'nombre' => $item->proyecto->nombre,
            ] : null,
            'rubro' => [
                'id'                => $item->plan_presupuestal->id,
                'codigo'            => $item->plan_presupuestal->codigo_rubro,
                'nombre'            => $item->plan_presupuestal->nombre_rubro,
                'codigo_con_nombre' => $item->plan_presupuestal->codigo_con_nombre,
                'plan_inicial'      => $item->plan_presupuestal->plan_inicial,
                'adiciones'         => $item->plan_presupuestal->adicciones,
                'reducciones'       => $item->plan_presupuestal->reducciones,
                'aplazamientos'     => $item->plan_presupuestal->aplazamientos,
                'creditos'          => $item->plan_presupuestal->creditos,
                'contra_creditos'   => $item->plan_presupuestal->contra_creditos,
                'reconocimientos'   => $item->plan_presupuestal->reconocimientos,
                'recaudos'          => $item->plan_presupuestal->recaudos,
                'disponibilidad'    => $item->plan_presupuestal->disponibilidad,
                'compromiso'        => $item->plan_presupuestal->compromiso,
                'obligacion'        => $item->plan_presupuestal->obligacion,
                'pagado'            => $item->plan_presupuestal->pagado,
                'cgr_chip'          => ($item->plan_presupuestal->codigo_cgr) ? [
                    'id'     => $item->plan_presupuestal->codigo_cgr->id,
                    'codigo' => $item->plan_presupuestal->codigo_cgr->codigo,
                    'nombre' => $item->plan_presupuestal->codigo_cgr->nombre,
                ] : null,
            ],
        ];

        return $output;
    }
}
