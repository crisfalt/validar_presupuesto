<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 15/02/2019
 * Time: 2:50 PM.
 */

namespace Modules\Presupuesto\Transformers;

class PreApropiacionInicialTransformer
{
    public function transformModel(Model $item)
    {
        $output = [
            'Vigencia'          => $item->vigencia ? $item->vigencia->nombre : '',
            'Codigo rubro'      => $item->codigo_rubro,
            'Nombre rubro'      => $item->nombre_rubro,
            'Maneja movimiento' => config('erp.lista_si_no.'.$item->maneja_movimiento),
            'Activo'            => config('erp.lista_si_no.'.$item->activo),
        ];

        return $output;
    }
}
