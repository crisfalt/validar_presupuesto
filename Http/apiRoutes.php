 <?php
/**
 * Created by PhpStorm.
 * User: crisfalt
 * Date: 29/08/2018
 * Time: 4:43 PM.
 */
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['middleware' => 'jwt.auth'], function ($api) {
    // Routes within this version group will require authentication.

    // Protected
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->group(['prefix' => 'prefuentefinanciamieno'], function ($api) { // Rutas para Institucion
            $api->get('/', [
                'uses'=> 'Modules\Presupuesto\Http\Controllers\Api\PreFuenteFinanciamientoController@index',
                // 'middleware' => 'can-api:Presupuesto.empresas.list'
            ]);
            $api->post('data', [
//                'as' => 'admin.Presupuesto.prvempresa.index',
                'uses'=> 'Modules\Presupuesto\Http\Controllers\Api\PreFuenteFinanciamientoController@getData',
                // 'middleware' => 'can-api:Presupuesto.empresas.list'
            ]);
            $api->get('{id}', [
                'uses'=> 'Modules\Presupuesto\Http\Controllers\Api\PreFuenteFinanciamientoController@show',
                // 'middleware' => 'can-api:Presupuesto.empresas.show'
            ]);
            $api->post('/', [
                'uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreFuenteFinanciamientoController@store',
                // 'middleware' => 'can-api:Presupuesto.empresas.create'
            ]);
            $api->put('{id}', [
                'uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreFuenteFinanciamientoController@update',
                // 'middleware' => 'can-api:Presupuesto.empresas.edit'
            ]);
            $api->delete('{id}', [
                'uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreFuenteFinanciamientoController@destroy',
                // 'middleware' => 'can-api:Presupuesto.empresas.destroy'
            ]);
        });
        $api->group(['prefix' => 'preplanpres'], function ($api) {// Rutas para Plan Presupuestal
            $api->get('index/{empresaId?}/{vigenciaId?}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@index']);
            $api->get('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@show']);
            $api->get('create/get', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@create']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@destroy']);
            $api->get('search/{query}', [
                'uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanPresupuestalController@searchAutoSuggest',
            ]);
        });
        // RUTAS PLAN FINANCIERO
        $api->group(['prefix' => 'preplanfinanciero'], function ($api) {// Rutas para Plan Presupuestal
            $api->get('index/{empresaId?}/{sedeId?}/{vigenciaId?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@show']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@destroy']);
        });
        //RUTAS INGRESOS VS GASTOS
        $api->group(['prefix' => 'preingresosgastos'], function ($api) {
            $api->get('index/{empresaId?}/{sedeId?}/{vigenciaId?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreIngresosGastosController@show']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PrePlanFinancieroController@destroy']);
        });
        // Rutas para Apropiacion Inicial
        $api->get('preapropiacion/index/{empresaId?}/{vigenciaId?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreApropiacionInicialController@index']);
        $api->get('preapropiacion/{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreApropiacionInicialController@show']);
        $api->post('preapropiacion', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreApropiacionInicialController@store']);
        $api->put('preapropiacion/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreApropiacionInicialController@update']);
        $api->delete('preapropiacion/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreApropiacionInicialController@destroy']);
        // Rutas para Solicitud de disponibilidad presupuestal
        $api->group(['prefix' => 'presolicidisp'], function ($api) {
            $api->get('/index/{vigenciaId?}/{sedeId?}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@index']);
            $api->post('/data/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@getData']);
            $api->get('/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@show']);
            $api->get('/report/index/{id?}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@report']);
            $api->post('', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@store']);
            $api->put('/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@update']);
            $api->delete('/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@destroy']);
            $api->get('/create/detail/{vigenciaId?}/{empresaId?}', 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@createDetail');
            $api->post('/generar_documento_posterior/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@generarDocumentoPosterior']);
            $api->post('/duplicar/{documento_id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@duplicate']);
            $api->post('/reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@reversar']);
        });
        // Rutas para la autorizacion Solicitud de disponibilidad presupuestal
        $api->get('preautorizacionscdp/index/{vigenciaId?}/{sedeId?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@index']);
        $api->post('preautorizacionscdp/data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@getData']);
        $api->get('preautorizacionscdp/{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@show']);
        $api->get('preautorizacionscdp/report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@report']);
        $api->post('preautorizacionscdp/{id_solicitud}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@store']);
        $api->put('preautorizacionscdp/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@update']);
        $api->delete('preautorizacionscdp/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@destroy']);
        $api->get('preautorizacionscdp/create/detail/{vigenciaId?}/{empresaId?}', 'Modules\Presupuesto\Http\Controllers\Api\PreAutorizacionSCDPController@createDetail');
        // Rutas para detalles presupuestales
        $api->get('predetallepresupuestal/index/{com_encabezado_documento_id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDetallePresupuestalController@index']);
        $api->post('predetallepresupuestal/data/{com_encabezado_documento_id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDetallePresupuestalController@getData']);
        $api->get('predetallepresupuestal/{id}/{tipoDocumento}/{vigenciaId?}/{institucionId?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDetallePresupuestalController@show']);
        $api->get('predetallepresupuestal/report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreSolicitudDisponibilidadController@report']);
        $api->post('predetallepresupuestal', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDetallePresupuestalController@store']);
        $api->put('predetallepresupuestal/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDetallePresupuestalController@update']);
        $api->delete('predetallepresupuestal/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDetallePresupuestalController@destroy']);
        // Rutas de disponibilidad presupuestal
        $api->get('predisponibilidad/index/{solicitud}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@index']);
        $api->post('predisponibilidad/data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@getData']);
        $api->get('predisponibilidad/{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@show']);
        $api->get('predisponibilidad/report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@report']);
        $api->post('predisponibilidad', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@store']);
        $api->put('predisponibilidad/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@update']);
        $api->delete('predisponibilidad/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@destroy']);
        $api->get('predisponibilidad/create/detail/{vigencia}/{empresa_id}', 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@createDetail');
        $api->post('predisponibilidad/generar_documento_posterior/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@generarDocumentoPosterior']);
        $api->post('predisponibilidad/duplicar/{documento_id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@duplicate']);
        $api->post('predisponibilidad/reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreDisponibilidadController@reversar']);
        $api->group(['prefix' => 'precompromiso'], function ($api) {// Rutas de compromiso
            $api->get('index/{solicitud}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@show']);
            $api->get('/report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@report']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@destroy']);
            $api->get('create/detail/{vigencia}/{empresa_id}', 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@createDetail');
            $api->post('generar_documento_posterior/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@generarDocumentoPosterior']);
            $api->post('duplicar/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@duplicate']);
            $api->post('reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreCompromisoController@reversar']);
        });
        $api->group(['prefix' => 'preobligacion'], function ($api) {// Rutas para Obligación Presupuestal
            $api->get('index/{solicitud}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@show']);
            $api->get('/report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@report']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@destroy']);
            $api->get('create/detail/{vigencia}/{empresa_id}', 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@createDetail');
            $api->post('generar_documento_posterior/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@generarDocumentoPosterior']);
            $api->post('duplicar/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@duplicate']);
            $api->post('reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreObligacionPresupuestalController@reversar']);
        });
        $api->group(['prefix' => 'pregiropresupuestal'], function ($api) {// Rutas para Giro Presupuestal
            $api->get('index/{solicitud}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@show']);
            $api->get('/report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@report']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@destroy']);
            $api->get('create/detail/{vigencia}/{empresa_id}', 'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@createDetail');
            $api->post('duplicar/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@duplicate']);
            $api->post('reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreGiroPresupuestalController@reversar']);
        });

        //api de documentos de ingresos
        $api->group(['prefix' => 'preingreso'], function ($api) {
            $api->get('index/{solicitud}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@show']);
            $api->post('get_expenses_income/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@getExpensesIncome']);
            $api->get('report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@report']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@destroy']);
            $api->get('create/detail/{vigencia}/{empresa_id}', 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@createDetail');
            $api->post('generar_documento_posterior/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@generarDocumentoPosterior']);
            $api->post('duplicar/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@duplicate']);
            $api->post('reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@reversar']);
            $api->get('/get_funding_sources/{rubroId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreIngresoController@getFundingSources']);
        });

        //api de documentos de modificaciones presupuestales
        $api->group(['prefix' => 'premodificacion'], function ($api) {
            $api->get('index/{solicitud}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@index']);
            $api->post('data/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@getData']);
            $api->get('{id}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@show']);
            $api->post('get_expenses_income/', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@getExpensesIncome']);
            $api->get('report/index/{id?}', ['uses'=>'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@report']);
            $api->post('/', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@store']);
            $api->put('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@update']);
            $api->delete('{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@destroy']);
            $api->get('create/detail/{vigencia}/{empresa_id}', 'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@createDetail');
            $api->post('duplicar/{referenciaId}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@duplicate']);
            $api->post('reverse/{id}', ['uses' => 'Modules\Presupuesto\Http\Controllers\Api\PreModificacionPresupuestalController@reversar']);
        });
    });
});
