<?php

namespace Modules\Presupuesto\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PreSolicitudDisponibilidadCreateRequest extends FormRequest
{
    /**
     * Get the failed validation response for the request.
     *
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        $transformed = [];

        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field'   => $field,
                'message' => $message[0],
            ];
        }

        return response()->json([
            'errors' => $transformed,
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'com_sede_id'              => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'          => 'required|exists:com_vigencias,id|integer',
            'com_documento_id'         => 'required|exists:com_documentos,id|integer',
            'descripcion'              => 'required|string',
            'numero_documento'         => 'required|integer',
            'fecha_documento'          => 'required|date',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'total'                    => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }
}
