<?php

namespace Modules\Presupuesto\Http\Controllers\Api;

use App\Rules\Money;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Modules\Comun\Entities\ComCentroCosto;
use Modules\Comun\Entities\ComDocumento;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Services\ComCentroCostoService;
use Modules\Comun\Services\ComEncabezadoDocumentoService;
use Modules\Comun\Transformers\EncabezadoDocumento\ComEncabezadoDocumentoTransformer;
use Modules\Comun\Transformers\Tercero\TerceroPresupuestoTransformer;
use Modules\Custom\Services\CustomService;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use Modules\Presupuesto\Entities\PrePlanPresupuestal;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\PlanPresupuestal\PreDetalleShowTransformer;
use Modules\User\Services\UserService;
use Validator;
use Yajra\DataTables\DataTables;

class PreModificacionPresupuestalController
{
    /**
     * @var ComEncabezadoDocumentoService
     */
    private $comEncabezadoDocumentoService;
    /**
     * @var PrePlanPresupuestalService
     */
    private $prePlanPresupuestalService;
    /**
     * @var ComCentroCostoService
     */
    private $comCentroCostoService;
    /**
     * @var CustomService
     */
    private $customService;

    /**
     * PreRecaudoController constructor.
     *
     * @param ComEncabezadoDocumentoService $comEncabezadoDocumentoService
     * @param PrePlanPresupuestalService    $prePlanPresupuestalService
     * @param ComCentroCostoService         $comCentroCostoService
     * @param CustomService                 $customService
     */
    public function __construct(ComEncabezadoDocumentoService $comEncabezadoDocumentoService,
                                PrePlanPresupuestalService $prePlanPresupuestalService,
                                ComCentroCostoService $comCentroCostoService,
                                CustomService $customService)
    {
        $this->comEncabezadoDocumentoService = $comEncabezadoDocumentoService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
        $this->comCentroCostoService = $comCentroCostoService;
        $this->customService = $customService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($vigenciaId = null, $sedeId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($sedeId) || $sedeId === 'null') {
            $sedeId = sedeActual()->id;
        }
        $idsDocumentos = [
            config('erp.maestros_documentos.api.id'),
            config('erp.maestros_documentos.rpi.id'),
            config('erp.maestros_documentos.ccg.id'),
            config('erp.maestros_documentos.ap.id'),
        ];

        return $this->comEncabezadoDocumentoService->obtenerDocumentos($vigenciaId, $sedeId, $idsDocumentos);
    }

    /**
     * Mostrar la lista de todos los datos activos de Tipos de Lista en un Datatable.
     *
     * @param Request
     *
     * @return Datatables
     */
    public function getData(Request $request)
    {
        $estados = config('erp.estado_documento');
        $documentosModificaciones = $this->index($request->com_vigencia_id, $request->com_sede_id);

        return datatables()->collection(collect(ComEncabezadoDocumentoTransformer::transform($documentosModificaciones)))
            ->addColumn('estado_documento', function ($documentos) use ($estados) {
                if ($documentos['estado'] == $estados['preliminar']) {
                    return  '<label class="text-center label label-success">'.trans('form.label.preliminary').'<label>';
                }
                if ($documentos['estado'] == $estados['confirmado'] || $documentos['estado'] == $estados['autorizado']) {
                    return  '<label class="text-center label label-info">'.trans('form.label.confirmed').'<label>';
                }
                if ($documentos['estado'] == $estados['anulado']) {
                    return  '<label class="text-center label label-danger">'.trans('form.label.canceled').'<label>';
                }
                if ($documentos['estado'] == $estados['rechazado']) {
                    return  '<label class="text-center label label-warning">'.trans('form.label.rejected').'<label>';
                }
            })
            ->with(['code' => 200, 'estados'=>$estados])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $idsDocumentos = [
            config('erp.maestros_documentos.api.id'),
            config('erp.maestros_documentos.rpi.id'),
            config('erp.maestros_documentos.ccg.id'),
            config('erp.maestros_documentos.ap.id'),
        ];
        $documentos = ComDocumento::withoutTrashed()
            ->whereIn('prv_maestro_documento_id', $idsDocumentos)
            ->get();
        $user = TerceroPresupuestoTransformer::transform(UserService::getTercero());
        $centros_costos = ComCentroCosto::withoutTrashed()->activo()->get(['id', 'nombre']);
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'documentos'     => $documentos,
                'user'           => $user,
                'centros_costos' => $centros_costos,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createDetail($vigenciaId = null, $empresaId = null)
    {
        if (is_null($vigenciaId)) {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId)) {
            $empresaId = empresaActual()->id;
        }
        $rubrosIngresos = $this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.ingresos'));
        $centrosCostos = $this->comCentroCostoService->obtenerCentrosCostos();
        $response = [
            'status'         => 'success',
            'code'           => 200,
            'rubros_gastos'  => $rubrosIngresos,
            'centros_costos' => $centrosCostos,
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        }
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money()],
        ];
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            return response()->json($this->comEncabezadoDocumentoService->store($request->all()));
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listas = self::create()->getData()->listas;
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => $listas,
            ];

            return response()->json($response);
        } else {
            $respuesta = $this->comEncabezadoDocumentoService->buscarDocumento($id, 'PRE')->getData(true);
            $respuesta['listas']['documentos'] = $listas->documentos;

            return response()->json($respuesta);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $rules = [
            'com_sede_id'              => 'required|exists:com_sedes,id|numeric|integer',
            'com_vigencia_id'          => 'required|exists:com_vigencias,id|numeric|integer',
            'com_documento_id'         => 'required|exists:com_documentos,id|integer',
            'descripcion'              => 'required|string',
            'numero_documento'         => 'required|numeric|integer',
            'fecha_documento'          => 'required|date',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money()],
        ];
        if ((int) $request->estado == config('erp.estado_documento.confirmado')) {
            $detalles_presupuestales = PreDetallePresupuestal::withoutTrashed()->where('com_encabezado_documento_id', $id)->first(); //get budgets details of document head
            if (is_null($detalles_presupuestales)) { // if not have details the head document return error
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => trans('messages.at_least_there_must_be_a_detail'),
                ];

                return response()->json($response);
            }
        }
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            try {
                DB::beginTransaction();
                $respuesta = $this->comEncabezadoDocumentoService->store($request->all(), $id);
                if ($respuesta['status'] == 'success') {
                    if ((int) $request->estado === config('erp.estado_documento.confirmado')) { //si la disponibilidad esta confirmada
                        foreach (PreDetallePresupuestal::withoutTrashed()->where('com_encabezado_documento_id', $id)->cursor() as $detalle) {
                            PrePlanPresupuestalService::cambiarOAsignarValorRubro($detalle->pre_plan_presupuestal_id, $detalle->valor, accionDocumento($request->com_documento_id), '+');
                        }
                    }
                }
                DB::commit();

                return response()->json($respuesta);
            } catch (Exception $exception) {
                DB::rollBack();
                Log::error('PreModificacionController::Error de excepcion metodo update no se pudo actualizar el documento '.$id.' en el archivo '.$exception->getFile().' en la linea '.$exception->getLine(), [$exception->getMessage()]);

                return response()->json(['msg' => $exception->getMessage(), 'status' => 'error', 'code'=> 403], 403);
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function reversar($id, Request $request)
    {
        $rules = [
            'id'            => 'required|exists:users,id|integer',
            'password'      => 'required|string',
            'justificacion' => 'required|min:20|max:300|string',
        ];
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            $response = $this->customService->validarAccionesAdministrativas($request);
            if ($response->getData(true)['status'] == 'success') {
                try {
                    DB::beginTransaction();
                    $response = $this->comEncabezadoDocumentoService->reversar($id);
                    if ($response->getData(true)['status'] == 'success') {
                        $detallesSolicitud = PreDetallePresupuestal::where('com_encabezado_documento_id', $id)->get();
                        foreach ($detallesSolicitud as $key => $detalle) {
                            PrePlanPresupuestalService::cambiarOAsignarValorRubro($detalle->pre_plan_presupuestal_id, $detalle->valor, 'compromiso ', '-');
                        }
                        DB::commit();
                        $response = [
                            'status' => 'success',
                            'msg'    => trans('comun.messages.document_reversed_successfully'),
                        ];

                        return response()->json($response);
                    } else {
                        return $response;
                    }
                } catch (\Exception $exception) {
                    DB::rollback(); // hago un rollback si hubo un error en alguna transacion con la base de datos
                    \Log::error('PreModificacionController::Error de excepcion metodo reversar', [$exception->getMessage()]);
                    $response = [
                        'status' => 'error',
                        'code'   => 403,
                        'msg'    => $exception->getMessage(),
                    ];

                    return response()->json($response);
                }
            } else {
                DB::rollBack();

                return $response;
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    /**
     * @param $documento_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicate($documento_id)
    {
        return $this->comEncabezadoDocumentoService->duplicar($documento_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return $this->comEncabezadoDocumentoService->destroy($id);
    }

    /**
     * @param null $id
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function report($id = null)
    {
        $reporte = Storage::disk('reportes')->get('reporte_scdp.mrt');
        $encabezado_documento = ComEncabezadoDocumento::withoutTrashed()
            ->where('id', $id)
            ->first()
            ->load('tercero1');
        $detalles_presupuestales = $encabezado_documento->detalles_presupuestales;
        $codigoQR = [
            'empresa_id'   => $encabezado_documento->sede->empresa->id,
            'sede_id'      => $encabezado_documento->sede->id,
            'documento_id' => $encabezado_documento->id,
        ];
        $data = [
            'encabezado_documento'    => $encabezado_documento->load('sede', 'sede.empresa'),
            'detalles_presupuestales' => $detalles_presupuestales,
//            'rubros_padre' => $this->arbolService->busquedaAbuelo( 647 ),
            'codigo_qr' => (!is_null($id)) ? $codigoQR : null,
        ];

        return response()->json(['data'=>$data, 'report'=>$reporte]);
    }

    /**
     * Obtener rubros gastos por rubro de ingreso.
     *
     * @param array $ingresosIds
     */
    public function getExpensesIncome(Request $request)
    {
        $rubros = [];
        $rules = [
            'ingresos_ids'                => 'required',
            'com_encabezado_documento_id' => 'required|exists:com_encabezados_documentos,id',
        ];
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            $ingresosIds = $request->ingresos_ids;
            if (!is_null($ingresosIds)) {
                foreach ($ingresosIds as $key => $ingresoId) {
                    $rubroIngreso = PrePlanPresupuestal::find($ingresoId);
                    $detalleRubro = $rubroIngreso->detalleRubro($request->com_encabezado_documento_id);
                    $rubros[$key]['rubro_ingreso'] = $rubroIngreso;
                    $rubros[$key]['rubro_ingreso']['valor'] = $detalleRubro['valor'];
                    $rubros[$key]['rubro_ingreso']['pre_detalle_presupuestal_id'] = $rubroIngreso->detalles_presupuestales
                        ->where('com_encabezado_documento_id', $request->com_encabezado_documento_id)->first()->id;
                    $rubros[$key]['rubro_ingreso']['rubros_gastos'] = PreDetalleShowTransformer::transform($detalleRubro->detalles_hijos);
                    unset($rubros[$key]['detalles_presupuestales']);
                }

                return response()->json(['status' => 'success', 'code' => 200, 'rubros' => $rubros]);
            }

            return response()->json([
                'status' => 'error', 'code' => 403,
                'msg'    => 'No se encontraron rubros de gastos asociados al de ingreso',
            ]);
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }
}
