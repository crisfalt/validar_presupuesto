<?php

namespace Modules\Presupuesto\Http\Controllers\Api;

use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Modules\Comun\Entities\ComDocumento;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Services\ComCentroCostoService;
use Modules\Comun\Services\ComEncabezadoDocumentoService;
use Modules\Comun\Transformers\EncabezadoDocumento\ComEncabezadoDocumentoTransformer;
use Modules\Presupuesto\Services\PreDetallePresupuestalService;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\Impresiones\PreImpresionDatatableTransformer;
use Modules\Presupuesto\Transformers\Impresiones\PreImpresionDocumentoTransformer;
use Modules\User\Services\UserService;
use Yajra\DataTables\DataTables;

class PreAutorizacionSCDPController extends Controller
{
    private $comEncabezadoDocumentoService;
    private $prePlanPresupuestalService;
    private $comCentroCostoService;
    private $preDetallePresupuestalService;

    public function __construct(ComEncabezadoDocumentoService $comEncabezadoDocumentoService,
                                 PrePlanPresupuestalService $prePlanPresupuestalService,
                                 ComCentroCostoService $comCentroCostoService,
                                PreDetallePresupuestalService $preDetallePresupuestalService)
    {
        $this->comEncabezadoDocumentoService = $comEncabezadoDocumentoService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
        $this->comCentroCostoService = $comCentroCostoService;
        $this->preDetallePresupuestalService = $preDetallePresupuestalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($vigenciaId = null, $sedeId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($sedeId) || $sedeId === 'null') {
            $sedeId = sedeActual()->id;
        }

        return $this->comEncabezadoDocumentoService->obtenerDocumentos($vigenciaId, $sedeId, [config('erp.maestros_documentos.ascdp.id')]);
    }

    /**
     * Mostrar la lista de todos los datos activos de Tipos de Lista en un Datatable.
     *
     * @param Request
     *
     * @return Datatables
     */
    public function getData(Request $request)
    {
        $estados = config('erp.estado_documento');
        $autorizaciones = $this->index($request->com_vigencia_id, $request->com_sede_id);

        return datatables()->collection(collect(ComEncabezadoDocumentoTransformer::transform($autorizaciones)))
            ->addColumn('estado_documento', function ($documentos) use ($estados) {
                if ($documentos['estado'] == $estados['preliminar']) {
                    return  '<label class="text-center label label-success">'.trans('form.label.preliminary').'<label>';
                }
                if ($documentos['estado'] == $estados['confirmado'] || $documentos['estado'] == $estados['autorizado']) {
                    return  '<label class="text-center label label-info">'.trans('form.label.confirmed').'<label>';
                }
                if ($documentos['estado'] == $estados['anulado']) {
                    return  '<label class="text-center label label-danger">'.trans('form.label.canceled').'<label>';
                }
                if ($documentos['estado'] == $estados['rechazado']) {
                    return  '<label class="text-center label label-warning">'.trans('form.label.rejected').'<label>';
                }
            })
            ->with(['code' => 200, 'estados'=>$estados])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $documentos = ComDocumento::withoutTrashed()->where('prv_maestro_documento_id', config('erp.maestros_documentos.ascdp.id'))->get();
        $response = [
            'status'     => 'success',
            'code'       => 200,
            'documentos' => $documentos,
        ];

        return response()->json($response);
    }

    /**
     * @param $idSCDP id de la solicitud a autorizar
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($idSCDP, Request $request)
    {
        $response = [];
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        }
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }

        try {
            if (is_null($idSCDP) || $idSCDP == '') {
                \Log::error('PreAutorizacionSCDPController::La id para buscar la solicitud esta vacia', [$idSCDP]);

                throw new \Exception(trans('messages.id_empty'));
            }
            $solicitudCDP = ComEncabezadoDocumento::withoutTrashed()->where('id', $idSCDP)->first();
            if (is_null($solicitudCDP)) {
                \Log::error('PreAutorizacionSCDPController::No se encontró el documento que se debe autorizar', [$idSCDP]);

                throw new \Exception(trans('comun.messages.document_not_found'));
            }
            if ($solicitudCDP->estado == config('erp.estado_documento.autorizado')) {
                throw new \Exception(trans('comun.messages.document_is_authorized'));
            }
            if ($solicitudCDP->estado == config('erp.estado_documento.rechazado')) {
                throw new \Exception(trans('comun.messages.document_is_rejected'));
            }
            // setear todas laas variables del request por los valores a almacenar
            $maetroDocumento = ComDocumento::withoutTrashed()->where('prv_maestro_documento_id', config('erp.maestros_documentos.ascdp.id'))->first();
            $request['com_documento_id'] = $maetroDocumento->id;
            $request['estado'] = config('erp.estado_documento.confirmado');
            $request['fecha_documento'] = Carbon::now()->format('Y-m-d H:i:s');
            $request['total'] = $solicitudCDP->total;
            $request['com_tercero1_id'] = UserService::getTercero(usuarioActual())->id;
            $request['documento_referencia_id'] = $solicitudCDP->id;
            $request['vigencia_presupuestal_id'] = ComEncabezadoDocumentoService::mapearVigenciaPresupuestal($solicitudCDP->vigencia_presupuestal_id);
            // validamos si la accion es autorizar el documento y se realizan las acciones si es el caso
            if ($request->estado_autorizacion == config('erp.estado_documento.autorizado')) {
                $request['descripcion'] = $solicitudCDP->descripcion;
            }
            // validamos si la accion es rechazar el documento y se realizan las acciones si es el caso
            if ($request->estado_autorizacion == config('erp.estado_documento.rechazado')) {
                $rules = [
                    'descripcion' => 'required|string|min:10',
                ];
                //lanzar validaciones
                $validator = Validator::make($request->all(), $rules);
                if (($validator->fails())) { // si las validaciones fallan retorna un array de errores
                    Log::error('PreAutorizacionSCDPController::Errores de validacion metodo store', $validator->errors()->toArray());
                    $response = [
                        'status' => 'error',
                        'code'   => 403,
                        'msg'    => $validator->errors(),
                    ];
                }
            }
            // actualizamos el estado de autorizacion a la SCDP
            $solicitudCDP->estado = $request->estado_autorizacion;
            DB::beginTransaction(); //iniciando transaciones con la base de datos
            $solicitudCDP->save(); //guardamos la solicitud
            unset($request['estado_autorizacion']); //se elimina el esatado autorizacion
            $response = $this->comEncabezadoDocumentoService->store($request->all());
            $autorizacionSCDP = ComEncabezadoDocumento::find($response['encabezado_documento']['id']);
            $this->preDetallePresupuestalService->copiarDetalles($solicitudCDP, $autorizacionSCDP);
            DB::commit(); //confirmo todas las transaciones
        } catch (Exception $exception) {
            DB::rollback(); // hago un rollback si hubo un error en alguna transacion con la base de datos
            Log::error('PreAutorizacionSCDPController::Error de excepcion metodo store', [$exception->getMessage().' Linea: '.$exception->getLine()]);
            $response = [
                'status' => 'error',
                'msg'    => $exception->getMessage(),
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => self::create(),
            ];

            return response()->json($response);
        } else {
            $respuesta = $this->comEncabezadoDocumentoService->buscarDocumento($id, 'PRE')->getData(true);
            $respuesta['listas']['documentos'] = self::create()->getData()->documentos;

            return response()->json($respuesta);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return $this->comEncabezadoDocumentoService->destroy($id);
    }

    public function report($id = null)
    {
        /* Obtenemos la bodega de reportes a traves del helper */
        $bodega = bodegaReportes();
        // Impresion de un documento el cual se busca por su id
        if (!is_null($id) && $id != '0') {
            if ($bodega->exists('reporte_com.mrt')) {
                // content of report
                $reporte = $bodega->get('reporte_com.mrt');
                $encabezado_documento = ComEncabezadoDocumento::withoutTrashed()
                    ->where('id', $id)
                    ->first();
                if (!is_null($encabezado_documento)) {
                    // data tranformed json to serialize in frontend
                    $data = PreImpresionDocumentoTransformer::transform($encabezado_documento);

                    return response()->json(['data'=>$data, 'report'=>$reporte]);
                }

                return response()->json(['status'=> 'error', 'msg'=>'No se encontro el documento']);
            }

            return response()->json(['status'=> 'error', 'msg'=> trans('No se encontro el archivo del reporte')]);
        }
        /* Para imprimir el datatable de solicitudes */
        if ($bodega->exists('reporte_datatable_autorizacion.mrt')) {
            $reporte = $bodega->get('reporte_datatable_autorizacion.mrt');
            $autorizaciones = $this->index();
            if (!$autorizaciones->isEmpty()) {
                $data = [
                    'encabezado_documento' => [
                        'nombre_maestro' => $autorizaciones[0]->maestro_documento->nombre,
                        'empresa'        => $autorizaciones[0]->empresa->nombre,
                        'sede'           => [
                            'nombre'    => $autorizaciones[0]->sede->nombre,
                            'telefono'  => $autorizaciones[0]->sede->telefono,
                            'direccion' => $autorizaciones[0]->sede->direccion,
                        ],
                    ],
                ];
                $data['registros'] = PreImpresionDatatableTransformer::transform($autorizaciones, [config('erp.maestros_documentos.ascdp.prefijo')]);

                return response()->json(['data'=>$data, 'report'=>$reporte]);
            }

            return response()->json(['status'=> 'error', 'msg'=> trans('form.datatable.language.no_record_found')]);
        }

        return response()->json(['status'=> 'error', 'msg'=> trans('No se encontro el archivo del reporte')]);
    }
}
