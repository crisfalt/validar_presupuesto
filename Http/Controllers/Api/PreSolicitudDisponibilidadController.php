<?php

namespace Modules\Presupuesto\Http\Controllers\Api;

use App\Rules\Money;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Comun\Entities\ComCentroCosto;
use Modules\Comun\Entities\ComDocumento;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Services\ComCentroCostoService;
use Modules\Comun\Services\ComEncabezadoDocumentoService;
use Modules\Comun\Transformers\EncabezadoDocumento\ComEncabezadoDocumentoTransformer;
use Modules\Comun\Transformers\Tercero\TerceroPresupuestoTransformer;
use Modules\Custom\Services\ArbolService;
use Modules\Custom\Services\CustomService;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\Impresiones\PreImpresionDatatableTransformer;
use Modules\Presupuesto\Transformers\Impresiones\PreImpresionDocumentoTransformer;
use Modules\User\Services\UserService;
use Validator;
use Yajra\Datatables\Datatables;

/**
 * Class PreSolicitudDisponibilidadController.
 */
class PreSolicitudDisponibilidadController extends Controller
{
    /**
     * @var ComEncabezadoDocumentoService
     */
    private $comEncabezadoDocumentoService;
    /**
     * @var PrePlanPresupuestalService
     */
    private $prePlanPresupuestalService;
    /**
     * @var ComCentroCostoService
     */
    private $comCentroCostoService;
    private $customService;
    private $arbolService;

    /**
     * PreSolicitudDisponibilidadController constructor.
     *
     * @param ComEncabezadoDocumentoService $comEncabezadoDocumentoService
     * @param PrePlanPresupuestalService    $prePlanPresupuestalService
     * @param ComCentroCostoService         $comCentroCostoService
     */
    public function __construct(ComEncabezadoDocumentoService $comEncabezadoDocumentoService,
                                PrePlanPresupuestalService $prePlanPresupuestalService,
                                ComCentroCostoService $comCentroCostoService,
                                CustomService $customService,
                                ArbolService $arbolService)
    {
        $this->comEncabezadoDocumentoService = $comEncabezadoDocumentoService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
        $this->comCentroCostoService = $comCentroCostoService;
        $this->customService = $customService;
        $this->arbolService = $arbolService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($vigenciaId = null, $sedeId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($sedeId) || $sedeId === 'null') {
            $sedeId = sedeActual()->id;
        }

        return $this->comEncabezadoDocumentoService->obtenerDocumentos($vigenciaId, $sedeId, [config('erp.maestros_documentos.scdp.id')]);
    }

    /**
     * Mostrar la lista de todos los datos activos de Tipos de Lista en un Datatable.
     *
     * @param Request
     *
     * @return Datatables
     */
    public function getData(Request $request)
    {
        $estados = config('erp.estado_documento');
        $solicitudes = $this->comEncabezadoDocumentoService->obtenerDocumentos(null, null, [config('erp.maestros_documentos.scdp.id')]);
        if (isset($request->componente)) {
            if ($request->componente == config('erp.maestros_documentos.cdp.prefijo')) {
                //$solicitudes = $solicitudes->where('estado', config('erp.estado_documento.autorizado') )->where('estado', config('erp.estado_documento.confirmado') );
                $solicitudes = $solicitudes->filter(function ($solicitud) {
                    return $solicitud->estado == config('erp.estado_documento.confirmado') && $solicitud->total <= config('erp.valor_tempora.total')
                        || $solicitud->estado == config('erp.estado_documento.autorizado') && $solicitud->total > config('erp.valor_tempora.total');
                });
            }
            if ($request->componente == config('erp.maestros_documentos.ascdp.prefijo')) {
                $solicitudes = $solicitudes->where('estado', config('erp.estado_documento.confirmado'))->where('total', '>', config('erp.valor_tempora.total'));
            }
        }

        return datatables()->collection(collect(ComEncabezadoDocumentoTransformer::transform($solicitudes)))
            ->addColumn('estado_documento', function ($documentos) use ($estados) {
                if ($documentos['estado'] == config('erp.estado_documento.preliminar')) {
                    return  '<label class="text-center label label-success">'.trans('form.label.preliminary').'<label>';
                }
                if ($documentos['estado'] == config('erp.estado_documento.confirmado')) {
                    return  '<label class="text-center label label-info">'.trans('form.label.confirmed').'<label>';
                }
                if ($documentos['estado'] == config('erp.estado_documento.autorizado')) {
                    return  '<label class="text-center label label-info">'.trans('presupuesto.form.label.authorized').'<label>';
                }
                if ($documentos['estado'] == config('erp.estado_documento.anulado')) {
                    return  '<label class="text-center label label-danger">'.trans('form.label.canceled').'<label>';
                }
                if ($documentos['estado'] == config('erp.estado_documento.rechazado')) {
                    return  '<label class="text-center label label-warning">'.trans('form.label.rejected').'<label>';
                }
            })
            ->with(['code' => 200, 'estados'=>$estados])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $documentos = ComDocumento::withoutTrashed()
            ->where('prv_maestro_documento_id', config('erp.maestros_documentos.scdp.id'))
            ->get(['id', 'nombre']);
        $user = TerceroPresupuestoTransformer::transform(UserService::getTercero());
        $centros_costos = ComCentroCosto::withoutTrashed()->activo()->get(['id', 'nombre']);
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'documentos'     => $documentos,
                'user'           => $user,
                'centros_costos' => $centros_costos,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createDetail($vigenciaId = null, $empresaId = null)
    {
        if (is_null($vigenciaId)) {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId)) {
            $empresaId = empresaActual()->id;
        }
        $rubrosGastos = $this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.gastos'));
        $centrosCostos = $this->comCentroCostoService->obtenerCentrosCostos();
        $response = [
            'status'         => 'success',
            'code'           => 200,
            'rubros_gastos'  => $rubrosGastos,
            'centros_costos' => $centrosCostos,
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        }
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date|date_format:Y-m-d',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'com_centro_costo_id'      => 'required|exists:com_centros_costos,id|integer',
            'com_tercero1_id'          => 'required|exists:com_terceros,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money()],
        ];
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            return response()->json($this->comEncabezadoDocumentoService->store($request->all()));
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listas = self::create()->getData()->listas;
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => $listas,
            ];

            return response()->json($response);
        } else {
            $respuesta = $this->comEncabezadoDocumentoService->buscarDocumento($id, 'PRE')->getData(true);
            $respuesta['listas']['documentos'] = $listas->documentos;
            $respuesta['listas']['user'] = $listas->user;
            $respuesta['listas']['centros_costos'] = $listas->centros_costos;

            return response()->json($respuesta);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date|date_format:Y-m-d',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'com_centro_costo_id'      => 'required|exists:com_centros_costos,id|integer',
            'com_tercero1_id'          => 'required|exists:com_terceros,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money()],
            'numero_documento'         => 'required|numeric|integer',
        ];
        if ((int) $request->estado == config('erp.estado_documento.confirmado')) {
            $detalles_presupuestales = PreDetallePresupuestal::withoutTrashed()->where('com_encabezado_documento_id', $id)->first(); //get budgets details of document head
            if (is_null($detalles_presupuestales)) { // if not have details the head document return error
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => trans('messages.at_least_there_must_be_a_detail'),
                ];

                return response()->json($response);
            }
        }
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            $respuesta = $this->comEncabezadoDocumentoService->store($request->all(), $id);
            if ($respuesta['status'] == 'success') {
                return response()->json($respuesta);
            } else {
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => 'Se genero un error al momento de actualizar la Solicitud',
                ];

                return response()->json($response);
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    public function reversar($id, Request $request)
    {
        $rules = [
            'id'            => 'required|exists:users,id|integer',
            'password'      => 'required|string',
            'justificacion' => 'required|min:20|max:300|string',
        ];
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            $response = $this->customService->validarAccionesAdministrativas($request);
            if ($response->getData(true)['status'] == 'success') {
                $response = $this->comEncabezadoDocumentoService->reversar($id);
                if ($response->getData(true)['status'] == 'success') {
                    $response = [
                        'status' => 'success',
                        'msg'    => trans('comun.messages.document_reversed_successfully'),
                    ];

                    return response()->json($response);
                } else {
                    return $response;
                }
            } else {
                return $response;
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    /**
     * Metodo para generar un documento posterior o que se genera automaticamente.
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function generarDocumentoPosterior($id, Request $request)
    {
        if (is_null($id)) {
            return response()->json(['status'=>'error', 'code'=>403, 'msg'=>trans('messages.id_empty')]);
        }
        if (is_null(ComEncabezadoDocumento::find($id))) {
            return response()->json(['status'=>'error', 'code'=>403, 'msg'=>trans('comun.messages.document_not_found')]);
        }

        return $this->comEncabezadoDocumentoService->generarDocumentoDesdeReferencia(sedeActual()->id, vigenciaActual()->id, $id, 5);
    }

    public function duplicate($documento_id)
    {
        return $this->comEncabezadoDocumentoService->duplicar($documento_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return $this->comEncabezadoDocumentoService->destroy($id);
    }

    /**
     * @param null $id
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function report($id = null)
    {
        /* Obtenemos la bodega de reportes a traves del helper */
        $bodega = bodegaReportes();
        /* Impresion de un documento el cual se busca por su id */
        if (!is_null($id) && $id != '0') {
            if ($bodega->exists('reporte_scdp.mrt')) {
                /* content of report */
                $reporte = $bodega->get('reporte_com.mrt');
                $encabezado_documento = ComEncabezadoDocumento::withoutTrashed()
                    ->where('id', $id)
                    ->first();
                if (!is_null($encabezado_documento)) {
                    /* data tranformed json to serialize in frontend */
                    $data = PreImpresionDocumentoTransformer::transform($encabezado_documento);

                    return response()->json(['data'=>$data, 'report'=>$reporte]);
                }

                return response()->json(['status'=> 'error', 'msg'=>'No se encontro el documento']);
            }

            return response()->json(['status'=> 'error', 'msg'=> trans('No se encontro el archivo del reporte')]);
        }
        /* Para imprimir el datatable de solicitudes */
        if ($bodega->exists('reporte_datatable_solicitud.mrt')) {
            $reporte = $bodega->get('reporte_datatable_solicitud.mrt');
            $autorizaciones = $this->index();
            if (!$autorizaciones->isEmpty()) {
                $data = [
                    'encabezado_documento' => [
                        'nombre_maestro' => $autorizaciones[0]->maestro_documento->nombre,
                        'empresa'        => $autorizaciones[0]->empresa->nombre,
                        'sede'           => [
                            'nombre'    => $autorizaciones[0]->sede->nombre,
                            'telefono'  => $autorizaciones[0]->sede->telefono,
                            'direccion' => $autorizaciones[0]->sede->direccion,
                        ],
                    ],
                ];
                $data['registros'] = PreImpresionDatatableTransformer::transform($autorizaciones, [config('erp.maestros_documentos.scdp.prefijo')]);

                return response()->json(['data'=>$data, 'report'=>$reporte]);
            }

            return response()->json(['status'=> 'error', 'msg'=> trans('form.datatable.language.no_record_found')]);
        }

        return response()->json(['status'=> 'error', 'msg'=> trans('No se encontro el archivo del reporte')]);
    }
}
