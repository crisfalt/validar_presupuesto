<?php

namespace Modules\Presupuesto\Http\Controllers\Api;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Comun\Entities\ComCgr;
use Modules\Comun\Entities\ComVigencia;
use Modules\Comun\Transformers\Cgrs\ComCgrShowTransformer;
use Modules\Custom\Services\ArbolService;
use Modules\Presupuesto\Entities\PrePlanPresupuestal;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\PlanPresupuestal\PreRubroShowTransformer;
use Modules\Privado\Entities\PrvListaElemento;
use PDOException;
use Validator;

class PrePlanPresupuestalController extends Controller
{
    private $arbolService;
    private $prePlanPresupuestalService;

    public function __construct(ArbolService $arbolService, PrePlanPresupuestalService $prePlanPresupuestalService)
    {
        $this->arbolService = $arbolService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($empresaId = null, $vigenciaId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId) || $empresaId === 'null') {
            $empresaId = empresaActual()->id;
        }
        $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal($empresaId, $vigenciaId);
        if (count($planPresupuestal) > 0) {
            $response = [
              'code'              => 200,
              'status'            => 'success',
              'plan_presupuestal' => $planPresupuestal,
            ];
        } else {
            $response = [
                'code'    => 403,
                'status'  => 'error',
                'message' => trans('messages.not_data'),
            ];
        }

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $vigencias = ComVigencia::withoutTrashed()->orderBy('nombre', 'DESC')->get(['id', 'nombre']);
        $tiposRubros = PrvListaElemento::withoutTrashed()->where('prv_lista_tipo_id', 4)->get(['id', 'codigo', 'nombre']);
        $cgrs = ComCgrShowTransformer::transform(ComCgr::withoutTrashed()->orderBy('codigo', 'ASC')->get(['id', 'codigo', 'nombre']));
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'vigencias'    => $vigencias,
                'tipos_rubros' => $tiposRubros,
                'cgrs'         => $cgrs,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws QueryException
     * @throws PDOException
     *
     * @return Response
     */
    public function store(Request $request, $id = null, $reglas = null)
    {
        if (is_null($reglas)) {
            $reglas = PrePlanPresupuestal::rules($request);
        }
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $reglas);
        if (!($validator->fails())) {
            //si todo esta bien se guarda el tipo de empresa
            if (is_null($id)) {
                $planPresupuestal = new PrePlanPresupuestal(); //nuevo registro
                $msg = trans('messages.created');
            } else {
                $planPresupuestal = PrePlanPresupuestal::find($id); //editar registro
                $msg = trans('messages.register_update');
            }
            $planPresupuestal->codigo_rubro = $request->input('codigo_rubro');
            $planPresupuestal->nombre_rubro = $request->input('nombre_rubro');
            $planPresupuestal->maneja_movimiento = $request->input('maneja_movimiento');
            $planPresupuestal->activo = $request->input('activo');
            if (!empty($request->input('prv_empresa_id'))) {
                $planPresupuestal->prv_empresa_id = $request->input('prv_empresa_id');
            }
            if (!empty($request->input('com_vigencia_id'))) {
                $planPresupuestal->com_vigencia_id = $request->input('com_vigencia_id');
            }
            if (!empty($request->input('tipo_rubro_id'))) {
                $planPresupuestal->tipo_rubro_id = $request->input('tipo_rubro_id');
            }
            if (!empty($request->input('pre_plan_presupuestal_id'))) {
                $planPresupuestal->pre_plan_presupuestal_id = $request->input('pre_plan_presupuestal_id');
            }
            if (!empty($request->input('com_cgr_id'))) {
                $planPresupuestal->com_cgr_id = $request->input('com_cgr_id');
            }

            try {
                $planPresupuestal->save();
                $response = [
                    'status' => 'success',
                    'code'   => 200,
                    'id'     => $planPresupuestal->id,
                    'msg'    => $msg,
                ];
                //llamar al servicio para reconstruir arbol presupuestal
                $arbol = $this->prePlanPresupuestalService->obtenerArbolConsulta($request->input('prv_empresa_id'), $request->input('com_vigencia_id'));
                $this->arbolService->regenerarArbol($arbol, 'pre_plan_presupuestal_id');
            } catch (QueryException $exception) { // error en una sentencia SQL
                \Log::error('PrePlanPresupuestalController::Error de base de datos metodo store', ['error'=>$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd'),
                ];
            } catch (PDOException $exception) { // error al buscar el driver de conexion
                \Log::error('PrePlanPresupuestalController::Error driver base de datos metodo store', ['error'=>$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.driver_not_found'),
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => $validator->errors(),
            ];
        }

        return response()->json($response);
    }

    /**
     * @param $query
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchAutoSuggest($query)
    {
        $rubros = PrePlanPresupuestal::search($query)->get(PrePlanPresupuestal::$resultColumns)->take(10); //buscador implementado con sofa/eloquence
        return response()->json($rubros);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listas = self::create()->getData()->listas;
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => $listas,
            ];
        } else {
            $rubro = $this->prePlanPresupuestalService->obtenerRubroPorId($id);
            if (!empty($rubro)) {
                $response = [
                    'status'                    => 'success',
                    'code'                      => 200,
                    'rubro'                     => PreRubroShowTransformer::transform($rubro),
                    'usuarios'                  => $rubro->usuarios()->get()->pluck('numero_documento_con_nombre_completo', 'id'),
                    'rubros_ingresos_vs_gastos' => ($rubro->tipo_rubro_id == config('erp.rubros.gastos')) ? $rubro->rubros_ingresos()->get()->pluck('codigo_con_nombre') : $rubro->rubros_gastos()->get()->pluck('codigo_con_nombre'),
                    'fuentes_financiamientos'   => $rubro->fuentes_financiamientos,
                    'listas'                    => $listas,
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.register_not_found'),
                ];
            }
        }

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        return self::store($request, $id, PrePlanPresupuestal::rules($request, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rubro = $this->prePlanPresupuestalService->obtenerRubroPorId($id);
        if (!empty($rubro)) {
            if ($rubro->hijos->count() > 0) {
                return response()->json(['status'=>'error', 'msg'=>trans('messages.item_budget_have_childrens')]);
            }
            // $rubro->delete();
            $rubro->forceDelete();
            $response = [
                'status' => 'success',
                'code'   => 200,
                'msg'    => trans('messages.register_deleted'),
            ];
        }

        return response()->json($response);
    }

    public function getFundingSources($rubroId)
    {
        if (!is_null($rubroId)) {
            $rubro = PrePlanPresupuestal::find($rubroId);
            if (!is_null($rubro)) {
                $fuentes = $this->prePlanPresupuestalService->fuentesPorRubro($rubro);

                return response()->json(['status'=>'success', 'code'=>200, 'fuentes_financiamientos'=>$fuentes]);
            } else {
                return response()->json(['status' => 'error', 'msg' => trans('messages.register_not_found')]);
            }
        } else {
            return response()->json(['status' => 'error', 'msg' => trans('messages.id_empty')]);
        }
    }
}
