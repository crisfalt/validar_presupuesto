<?php

namespace Modules\Presupuesto\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Services\ComCentroCostoService;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use Modules\Presupuesto\Entities\PreFuenteFinanciamiento;
use Modules\Presupuesto\Entities\PreProyecto;
use Modules\Presupuesto\Services\PreDetallePresupuestalService;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\PlanPresupuestal\PreDetalleShowTransformer;
use Modules\Presupuesto\Transformers\PlanPresupuestal\PreRubroShowTransformer;
use Validator;
use Yajra\Datatables\Datatables;

class PreDetallePresupuestalController extends Controller
{
    private $preDetallePresupuestalService;
    private $prePlanPresupuestalService;
    private $comCentroCostoService;

    public function __construct(PreDetallePresupuestalService $preDetallePresupuestalService, PrePlanPresupuestalService $prePlanPresupuestalService,
                                    ComCentroCostoService $comCentroCostoService)
    {
        $this->preDetallePresupuestalService = $preDetallePresupuestalService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
        $this->comCentroCostoService = $comCentroCostoService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($com_encabezado_documento_id)
    {
        if (is_null($com_encabezado_documento_id)) {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => 'el id de encabezado documento no debe ser vacio',
            ];

            return response()->json($response);
        } else {
            return PreDetalleShowTransformer::transform($this->preDetallePresupuestalService->obtenerDetalles($com_encabezado_documento_id));
        }
    }

    /**
     * Mostrar la lista de todos los datos activos de Tipos de Lista en un Datatable.
     *
     * @return Response
     */
    public function getData($com_encabezado_documento_id)
    {
        $detallesPresupuestales = self::index($com_encabezado_documento_id);

        return datatables()->collection(collect($detallesPresupuestales))
            ->with(['code' => 200])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($tipoDocumento, $encabezadoDocumentoId, $vigenciaId = null, $empresaId = null)
    {
        /* Variables */
        $rubros = [];
        if (is_null($vigenciaId) || $vigenciaId == 'null' || $vigenciaId = 'NULL' || $vigenciaId == 0) {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId) || $empresaId == 'null' || $empresaId = 'NULL' || $empresaId == 0) {
            $empresaId = empresaActual()->id;
        }
        switch (config('erp.maestros_documentos.'.strtolower($tipoDocumento).'.accion')) {
            case  'ingreso':
                $rubros['rubros_ingresos'] = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.ingresos')));
                break;
            case  'gasto':
                $rubros['rubros_gastos'] = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.gastos')));
                break;
            case  'modificacion':
                if (config('erp.maestros_documentos.'.$tipoDocumento.'.prefijo') == 'api' || config('erp.maestros_documentos.'.$tipoDocumento.'.prefijo') == 'rpi') {
                    $rubros['rubros_ingresos'] = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.ingresos')));
                    $rubros['rubros_gastos'] = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.gastos')));
                } else {
                    $rubros['rubros_gastos'] = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigenciaId, $empresaId, config('erp.rubros.gastos')));
                }
                break;
        }
        $centrosCostos = $this->comCentroCostoService->obtenerCentrosCostos();
        $proyectos = PreProyecto::withoutTrashed()->get();
        $fuentesFinanciamiento = PreFuenteFinanciamiento::withoutTrashed()->get();
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'rubros'                  => $rubros,
                'centros_costos'          => $centrosCostos,
                'proyectos'               => $proyectos,
                'fuentes_financiamientos' => $fuentesFinanciamiento,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), PreDetallePresupuestal::$rules);
        if (!($validator->fails())) {
            //si todo esta bien se guarda el tipo de empresa
            try {
                // get document head
                $encabezado_documento = ComEncabezadoDocumento::withoutTrashed()->find($request->com_encabezado_documento_id);

                $detallePresupuestal = new PreDetallePresupuestal();
                // add document references and thirdy if head have this
                $request['documento_referencia_id'] = ($request['documento_referencia_id']) ? $request['documento_referencia_id'] : $encabezado_documento->documento_referencia_id;
                $request['com_tercero_id'] = ($request['com_tercero_id']) ? $request['com_tercero_id'] : $encabezado_documento->com_tercero_id;
                /* Agregar descripcion y centro de costo al detalle del encabezado si vienen vacios */
                if (!isset($request['com_centro_costo_id']) || is_null($request['com_centro_costo_id'])) {
                    $request['com_centro_costo_id'] = ($encabezado_documento->com_centro_costo_id) ? $encabezado_documento->com_centro_costo_id : 1;
                }
                if (!isset($request['descripcion']) || is_null($request['descripcion'])) {
                    $request['descripcion'] = $encabezado_documento->descripcion;
                }
                $detallePresupuestal->forceFill($request->all())->save();
                $detalles = $encabezado_documento->detalles_presupuestales;
                $response = [
                    'status'                      => 'success',
                    'code'                        => 200,
                    'detalles'                    => Datatables::of($detalles)->make(true),
                    'pre_detalle_presupuestal_id' => $detallePresupuestal->id,
                    'msg'                         => trans('messages.created'),
                ];
            } catch (Illuminate\Database\QueryException $e) { // error en una sentencia SQL
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd').$e,
                ];
            } catch (PDOException $e) { // error al buscar el driver de conexion
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.driver_not_found').$e,
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => $validator->errors(),
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id, $tipoDocumento, $vigenciaId = null, $institucionId = null)
    {
        $listas = self::create($tipoDocumento, $vigenciaId, $institucionId)->getData()->listas;
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => $listas,
            ];

            return response()->json($response);
        } else {
            $detallePresupuestal = PreDetallePresupuestal::findOrFail($id);
            if (!empty($detallePresupuestal)) {
                $response = [
                    'status'               => 'success',
                    'code'                 => 200,
                    'detalle_presupuestal' => PreDetalleShowTransformer::transform($detallePresupuestal),
                    'listas'               => $listas,
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.register_not_found'),
                ];
            }

            return response()->json($response);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), PreDetallePresupuestal::$rules);
        if (!($validator->fails())) {
            //si todo esta bien se guarda el tipo de empresa
            $detallePresupuestal = PreDetallePresupuestal::findOrFail($id);

            try {
                $detallePresupuestal->forceFill($request->all())->save();
                $response = [
                    'status'                      => 'success',
                    'code'                        => 200,
                    'msg'                         => trans('messages.register_update'),
                    'pre_detalle_presupuestal_id' => $detallePresupuestal->id,
                ];
            } catch (Illuminate\Database\QueryException $exception) { // error en una sentencia SQL
                Log::error('PreDetallePresupuestalController::Error de excepcion metodo update no se pudo actualizar el detalle '.$id.' en el archivo '.$exception->getFile().' en la linea '.$exception->getLine(), [$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd').$exception,
                ];
            } catch (\PDOException $exception) { // error al buscar el driver de conexion
                Log::error('PreDetallePresupuestalController::Error de excepcion metodo update no se pudo actualizar el detalle '.$id.' en el archivo '.$exception->getFile().' en la linea '.$exception->getLine(), [$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.driver_not_found').$exception,
                ];
            } catch (\Exception $exception) { // error en una sentencia SQL
                Log::error('PreDetallePresupuestalController::Error de excepcion metodo update no se pudo actualizar el detalle '.$id.' en el archivo '.$exception->getFile().' en la linea '.$exception->getLine(), [$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd').$exception,
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => $validator->errors(),
            ];
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detallePresupuestal = PreDetallePresupuestal::find($id);
        if (!empty($detallePresupuestal)) {
            //eliminar en cascada , delete user by id
            $detallePresupuestal->delete(); //ELIMINAR, DELETE
            //arreglo con datos o mensajes de respueta, array with datas o messages to return
            $response = [
                'status' => 'success',
                'code'   => 200,
                'msg'    => trans('messages.register_deleted'),
            ];
        } else {
            $response = [
                'status' => 'error',
                'msg'    => trans('messages.register_not_found'),
            ];
        }

        return response()->json($response);
    }
}
