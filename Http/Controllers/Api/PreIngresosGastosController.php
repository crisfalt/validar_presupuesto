<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 8/04/2019
 * Time: 9:31 AM.
 */

namespace Modules\Presupuesto\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Custom\Services\ArbolService;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\PlanPresupuestal\PreRubroShowTransformer;

class PreIngresosGastosController extends Controller
{
    private $arbolService;
    private $prePlanPresupuestalService;

    public function __construct(ArbolService $arbolService, PrePlanPresupuestalService $prePlanPresupuestalService)
    {
        $this->arbolService = $arbolService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $empresa_id = auth()->user()->empresa_logueada->id;
        $vigencia_id = auth()->user()->vigencia_logueada->id;
        $rubros_ingresos = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigencia_id, $empresa_id, config('erp.rubros.ingresos')));
        $rubros_gastos = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigencia_id, $empresa_id, config('erp.rubros.gastos')));
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'rubros_ingresos' => $rubros_ingresos,
                'rubros_gastos'   => $rubros_gastos,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listas = self::create()->getData()->listas;
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => $listas,
            ];
        } else {
            $fuente = PreFuenteFinanciamiento::findOrFail($id);
            if (!empty($fuente)) {
                $response = [
                    'status' => 'success',
                    'code'   => 200,
                    'fuente' => $fuente->load('rubros'),
                    'listas' => $listas,
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.register_not_found'),
                ];
            }
        }

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request, $id = null, $reglas = null)
    {
        if (is_null($reglas)) {
            $reglas = PreFuenteFinanciamiento::rules($request);
        }
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $reglas);
        if (!($validator->fails())) {
            //si todo esta bien se guarda el tipo de empresa
            if (is_null($id)) {
                $fuente = new PreFuenteFinanciamiento(); //nuevo registro
                $msg = trans('messages.created');
            } else {
                $fuente = PreFuenteFinanciamiento::find($id); //editar registro
                $msg = trans('messages.register_update');
            }
            $fuente->codigo = $request->input('codigo');
            $fuente->nombre = $request->input('nombre');
            $fuente->descripcion = $request->input('descripcion');
            $fuente->activo = $request->input('activo');

            try {
                $fuente->save();
                if (!is_null($request->input('rubros'))) {
                    if (is_null($id)) {
                        $fuente->rubros()->attach($request->input('rubros'));
                    } else {
                        $fuente->rubros()->sync($request->input('rubros'));
                    }
                }
                $response = [
                    'status' => 'success',
                    'code'   => 200,
                    'msg'    => $msg,
                ];
            } catch (Illuminate\Database\QueryException $e) { // error en una sentencia SQL
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd').$e,
                ];
            } catch (PDOException $e) { // error al buscar el driver de conexion
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.driver_not_found').$e,
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => $validator->errors(),
            ];
        }

        return response()->json($response);
    }
}
