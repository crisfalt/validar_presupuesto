<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 3/04/2019
 * Time: 3:16 PM.
 */

namespace Modules\Presupuesto\Http\Controllers\Api;

use App\Rules\Money;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Comun\Entities\ComCentroCosto;
use Modules\Comun\Entities\ComDocumento;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Services\ComCentroCostoService;
use Modules\Comun\Services\ComEncabezadoDocumentoService;
use Modules\Comun\Transformers\EncabezadoDocumento\ComEncabezadoDocumentoTransformer;
use Modules\Comun\Transformers\Tercero\TerceroPresupuestoTransformer;
use Modules\Custom\Services\CustomService;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\User\Services\UserService;
use Validator;
use Yajra\Datatables\Datatables;

class PrePlanFinancieroController extends Controller
{
    private $comEncabezadoDocumentoService;
    private $prePlanPresupuestalService;
    private $comCentroCostoService;
    private $customService;

    public function __construct(ComEncabezadoDocumentoService $comEncabezadoDocumentoService,
                                 PrePlanPresupuestalService $prePlanPresupuestalService,
                                 ComCentroCostoService $comCentroCostoService,
                                 CustomService $customService)
    {
        $this->comEncabezadoDocumentoService = $comEncabezadoDocumentoService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
        $this->comCentroCostoService = $comCentroCostoService;
        $this->customService = $customService;
    }

    /* Display a listing of the resource.
    *
    * @return Response
    */
    public function index($empresaId = null, $sedeId = null, $vigenciaId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId) || $empresaId === 'null') {
            $empresaId = empresaActual()->id;
        }
        if (is_null($sedeId) || $sedeId === 'null') {
            $sedeId = sedeActual()->id;
        }
        $tercero = UserService::getTercero();
        $planFinanciero = ComEncabezadoDocumento::withoutTrashed()
            ->where('com_vigencia_id', $vigenciaId)
            ->where('prv_empresa_id', $empresaId)
            ->where('com_sede_id', $sedeId)
            //->where('com_centro_costo_id', $empresaId)
            ->where('com_documento_id', 23)
            ->where('com_tercero1_id', $tercero->id)
            ->first();
        if (is_null($planFinanciero)) {
            return self::create();
        } else {
            $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal($empresaId, $vigenciaId);
            $detallesPlanFinanciero = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $planFinanciero->detalles_presupuestales);
            $response = [
                'status'                     => 'success',
                'code'                       => 200,
                'encabezado_plan_financiero' => ComEncabezadoDocumentoTransformer::transform($planFinanciero),
                'detalles_plan_financiero'   => $detallesPlanFinanciero,
                'listas'                     => self::create()->getData()->listas,
            ];

            return response()->json($response);
        }
    }

    public function getData(Request $request)
    {
        if (is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        }
        if (is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }
        $estados = config('erp.estado_documento');
        $planesFinancieros = $this->comEncabezadoDocumentoService->obtenerDocumentos($request->com_vigencia_id, $request->com_sede_id, [config('erp.maestros_documentos.pf.id')]);

        return datatables()->collection(collect(ComEncabezadoDocumentoTransformer::transform($planesFinancieros)))
            ->addColumn('estado_documento', function ($documentos) use ($estados) {
                if ($documentos['estado'] == $estados['preliminar']) {
                    return  '<label class="text-center label label-success">'.trans('form.label.preliminary').'<label>';
                }
                if ($documentos['estado'] == $estados['confirmado'] || $documentos['estado'] == $estados['autorizado']) {
                    return  '<label class="text-center label label-info">'.trans('form.label.confirmed').'<label>';
                }
                if ($documentos['estado'] == $estados['anulado']) {
                    return  '<label class="text-center label label-danger">'.trans('form.label.canceled').'<label>';
                }
                if ($documentos['estado'] == $estados['rechazado']) {
                    return  '<label class="text-center label label-warning">'.trans('form.label.rejected').'<label>';
                }
            })
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $documentos = ComDocumento::withoutTrashed()->where('prv_maestro_documento_id', config('erp.maestros_documentos.pf.id'))->orderBy('nombre')->get();
        $fecha = Carbon::now()->format('d-m-Y');
        $user = TerceroPresupuestoTransformer::transform(UserService::getTercero());
        $centros_costos = ComCentroCosto::withoutTrashed()->activo()->get(['id', 'nombre']);
//        $actosAdministrativos = ComActoAdministrativo::withoutTrashed()->where( 'com_documento_id', '1' )->where( 'com_vigencia_id', $user->vigencia_logueada->id )->where( 'valor', '>', '0' )->first();
//        if( is_null( $actosAdministrativos ) ) return response()->json(['status'=>'error','msg'=>'Debe existir un acuerdo del plan presupuestal para la vigencia']);
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'documentos'     => $documentos,
                'fecha'          => $fecha,
                'usuario'        => $user,
                'centros_costos' => $centros_costos,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createDetail()
    {
        /* Obtener usuario logueado */
        $user = auth()->user();
        $rubros = $user->rubros;
        $response = [
            'status' => 'success',
            'code'   => 200,
            'rubros' => $rubros,
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        } //cambiar valor de request
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }
        $tercero = UserService::getTercero();
        $buscarDuplicidad = ComEncabezadoDocumento::withoutTrashed()
            ->where('com_sede_id', $request->com_sede_id)
            ->where('com_vigencia_id', $request->com_vigencia_id)
            //->where('com_centro_costo_id', $empresaId)
            ->where('com_documento_id', 23)
            ->where('com_tercero1_id', $tercero->id)
            ->first();
        if (!is_null($buscarDuplicidad)) {
            $response = [
                'status'               => 'error',
                'code'                 => 403,
                'encabezado_documento' => $buscarDuplicidad,
                'msg'                  => 'Ya existe un plan financiero en la depedencia y vigencia seleccionada',
            ];
            $rubrosPlanPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal(empresaActual()->id, $request['com_vigencia_id']);
            $detallesPlanFinanciero = $this->preDetallePresupuestalService->obtenerDetalles($buscarDuplicidad->id);
            if ($detallesPlanFinanciero->count() > 0) {
                $detallesCombinados = $this->prePlanPresupuestalService->combinarPlanConDetalles($rubrosPlanPresupuestal, $detallesPlanFinanciero->toArray(), $buscarDuplicidad->estado);
            }
            $response['detalles_apropiacion'] = $detallesCombinados;

            return response()->json($response);
        }
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date|date_format:"Y-m-d"',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'com_centro_costo_id'      => 'required|exists:com_centros_costos,id|integer',
            'com_tercero1_id'          => 'required|exists:com_terceros,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money()],
        ];
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            if (isset($request['rubros'])) {
                unset($request['rubros']);
            }

            try {
                //Iniciado las transaciones
                DB::beginTransaction();
                /* Asignar empresa al documento */
                $request['prv_empresa_id'] = empresaActual()->id;
                $response = $this->comEncabezadoDocumentoService->store($request->all());
                $planFinanciero = ComEncabezadoDocumento::findOrFail(((object) $response['encabezado_documento'])->id);
                $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal(empresaActual()->id, $request['com_vigencia_id']);
                $rubrosUsuario = usuarioActual()->rubros;
                foreach ($rubrosUsuario as $rubro) {
                    $detallesPlanFinanciero = new PreDetallePresupuestal();
                    $detallesPlanFinanciero->com_centro_costo_id = 1;
                    $detallesPlanFinanciero->pre_plan_presupuestal_id = $rubro->id;
                    $detallesPlanFinanciero->descripcion = $planFinanciero->descripcion;
                    $detallesPlanFinanciero->documento_referencia_id = $planFinanciero->id;
                    $planFinanciero->detalles_presupuestales()->save($detallesPlanFinanciero);
                }
//                $response['detalles_plan_financiero'] = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $planFinanciero->detalles_presupuestales, $planFinanciero->estado);
                $response['detalles_plan_financiero'] = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $planFinanciero->detalles_presupuestales);
                DB::commit(); //confirmo todas las transaciones
                return response()->json($response);
            } catch (\Exception $exception) {
                DB::rollback(); // hago un rollback si hubo un error en alguna transacion con la base de datos
                \Log::error('PrePlaFinancieroController::Error de excepcion metodo save', [$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => $exception->getMessage(),
                ];

                return response()->json($response);
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    private function validarIgualdadPacEnRubros($rubros)
    {
        foreach ($rubros as $key => $rubro) {
            $pacs = $rubro['pac'];
            $sumatoria = 0;
            foreach ($pacs as $key => $pac) {
                $sumatoria += $pac;
            }
            if (CustomService::formatearDecimales($sumatoria, 2, ',', '.') != CustomService::formatearDecimales($rubro['valor'], 2, ',', '.')) {
                return false;
            }
        }

        return true;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function actualizarDetallesApropiacion($encabezadoDocumento, $datos, $rubro)
    {
        $detallePresupuestal = PreDetallePresupuestal::withoutTrashed()->where('pre_plan_presupuestal_id', $rubro['id'])->first();
        $detallePresupuestal->com_encabezado_documento_id = $encabezadoDocumento->id;
        $detallePresupuestal->com_centro_costo_id = 2;
        $detallePresupuestal->descripcion = $datos['descripcion'];
        $detallePresupuestal->valor = $rubro['valor'];
        $detallePresupuestal->documento_referencia_id = $encabezadoDocumento->id;
        for ($i = 1; $i <= 12; $i++) {
            if ($i < 10) {
                $detallePresupuestal['pac0'.$i] = $rubro['pac'][($i - 1)];
            } else {
                $detallePresupuestal['pac'.$i] = $rubro['pac'][($i - 1)];
            }
        }
        $encabezadoDocumento->detalles_presupuestales()->save($detallePresupuestal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        } //cambiar valor de request
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }
        $tercero = UserService::getTercero();
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date|date_format:"Y-m-d"',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'com_centro_costo_id'      => 'required|exists:com_centros_costos,id|integer',
            'com_tercero1_id'          => 'required|exists:com_terceros,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money()],
        ];
        if ($request->estado == 1 || $request->estado == '1') { //si el estado es confirmar
            $rules['rubros'] = 'required'; //agrego la validacion para rubros
        }
        //lanzar validaciones,launch validates to User
        $msg = '';
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            $rubros = json_decode($request->rubros, true);
            if (!self::validarIgualdadPacEnRubros($rubros)) {
                $msg = trans('form.label.pac_not_match');
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => $msg,
                ];

                return response()->json($response);
            }
            unset($request['rubros']);

            try {
                //Iniciado las transaciones
                DB::beginTransaction();
                $respuesta = $this->comEncabezadoDocumentoService->store($request->all(), $id);
                if ($respuesta['status'] === 'success') {
                    $planFinanciero = (object) $respuesta['encabezado_documento'];
                    $planFinanciero = ComEncabezadoDocumento::find($planFinanciero->id);
                    if (!is_null($rubros)) {
                        foreach ($rubros as $key => $rubro) {
                            self::actualizarDetallesApropiacion($planFinanciero, $request->all(), $rubro);
                        }
//                        foreach ($rubros as $rubro) {
//                            $detallesPlanFinanciero = PreDetallePresupuestal::withoutTrashed()->where('com_encabezado_documento_id', $planFinanciero->id)->first();
//                            $detallesPlanFinanciero->com_centro_costo_id = 1;
//                            $detallesPlanFinanciero->pre_plan_presupuestal_id = $rubro->id;
//                            $detallesPlanFinanciero->descripcion = $planFinanciero->descripcion;
//                            $detallesPlanFinanciero->documento_referencia_id = $planFinanciero->id;
//                            $planFinanciero->detalles_presupuestales()->save($detallesPlanFinanciero);
//                        }
                    }
                    DB::commit(); //confirmo todas las transaciones
                    $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal(empresaActual()->id, $request['com_vigencia_id']);
                    $respuesta['detalles_apropiacion'] = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $planFinanciero->detalles_presupuestales);
                }

                return response()->json($respuesta);
            } catch (\Exception $exception) {
                DB::rollback(); // hago un rollback si hubo un error en alguna transacion con la base de datos
                \Log::error('PrePlanFinancieroController::Error de excepcion metodo update', [$exception->getMessage().' linea '.$exception->getLine()]);
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => $exception->getMessage().' linea '.$exception->getLine(),
                ];

                return response()->json($response);
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }
}
