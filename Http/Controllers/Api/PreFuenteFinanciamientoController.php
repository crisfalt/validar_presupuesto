<?php
/**
 * Created by PhpStorm.
 * User: crisf
 * Date: 13/03/2019
 * Time: 10:38 PM.
 */

namespace Modules\Presupuesto\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Custom\Services\CustomService;
use Modules\Presupuesto\Entities\PreFuenteFinanciamiento;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Transformers\PlanPresupuestal\PreRubroShowTransformer;
use Validator;
use Yajra\Datatables\Datatables;

class PreFuenteFinanciamientoController extends Controller
{
    private $prePlanPresupuestalService;

    public function __construct(PrePlanPresupuestalService $prePlanPresupuestalService)
    {
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $fuentes = PreFuenteFinanciamiento::withoutTrashed();
        if (isset(PreFuenteFinanciamiento::$defaultOrder, PreFuenteFinanciamiento::$directionOrder)) {
            for ($i = 0; $i < count(PreFuenteFinanciamiento::$defaultOrder); $i++) {
                $fuentes->orderBy(PreFuenteFinanciamiento::$defaultOrder[$i], PreFuenteFinanciamiento::$directionOrder[$i]);
            }
        }
        $fuentes = $fuentes->get();
        if (!empty($fuentes)) {
            $response = [
                'code'    => 200,
                'fuentes' => $fuentes,
            ];
        } else {
            $response = [
                'code' => 204,
                'msg'  => trans('messages.not_data'),
            ];
        }

        return response()->json($response);
    }

    /**
     * Mostrar la lista de todos los datos activos de Tipos de Lista en un Datatable.
     *
     * @return Response
     */
    public function getData()
    {
        $fuentes = PreFuenteFinanciamiento::withoutTrashed();

        return Datatables::of($fuentes)
            ->order(function ($query) {
                if (isset(PreFuenteFinanciamiento::$defaultOrder, PreFuenteFinanciamiento::$directionOrder)) {
                    for ($i = 0; $i < count(PreFuenteFinanciamiento::$defaultOrder); $i++) {
                        $query->orderBy(PreFuenteFinanciamiento::$defaultOrder[$i], PreFuenteFinanciamiento::$directionOrder[$i]);
                    }
                }
            })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $empresa_id = empresaActual()->id;
        $vigencia_id = vigenciaActual()->id;
        $rubros_ingresos = PreRubroShowTransformer::transform($this->prePlanPresupuestalService->obtenerRubrosPorTipo($vigencia_id, $empresa_id, config('erp.rubros.ingresos')));
        $response = [
            'listas' => [
                'rubros_ingresos' => $rubros_ingresos,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request, $id = null, $reglas = null)
    {
        if (is_null($reglas)) {
            $reglas = PreFuenteFinanciamiento::rules($request);
        }
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $reglas);
        if (!($validator->fails())) {
            //si todo esta bien se guarda el tipo de empresa
            if (is_null($id)) {
                $fuente = new PreFuenteFinanciamiento(); //nuevo registro
                $msg = trans('messages.created');
            } else {
                $fuente = PreFuenteFinanciamiento::find($id); //editar registro
                $msg = trans('messages.register_update');
            }
            $fuente->codigo = $request->input('codigo');
            $fuente->nombre = $request->input('nombre');
            $fuente->descripcion = $request->input('descripcion');
            $fuente->activo = $request->input('activo');

            try {
                $fuente->save();
                if (!is_null($request->input('rubros'))) {
                    if (is_null($id)) {
                        $fuente->rubros()->attach($request->input('rubros'));
                    } else {
                        $fuente->rubros()->sync($request->input('rubros'));
                    }
                }
                $response = [
                    'status' => 'success',
                    'code'   => 200,
                    'msg'    => $msg,
                ];
            } catch (Illuminate\Database\QueryException $e) { // error en una sentencia SQL
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd').$e,
                ];
            } catch (PDOException $e) { // error al buscar el driver de conexion
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.driver_not_found').$e,
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => $validator->errors(),
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listas = self::create()->getData()->listas;
        if (is_null($id) || $id == 0) {
            $response = [
                'status' => 'success',
                'code'   => 200,
                'listas' => $listas,
            ];
        } else {
            $fuente = PreFuenteFinanciamiento::findOrFail($id);
            if (!empty($fuente)) {
                $response = [
                    'status' => 'success',
                    'code'   => 200,
                    'fuente' => $fuente->load('rubros'),
                    'listas' => $listas,
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.register_not_found'),
                ];
            }
        }

        return response()->json($response);
    }

    public function export(Request $request)
    {
        $modelo = new PreFuenteFinanciamiento();
        $response = CustomService::exportar($request, $modelo);
        $urlArchivo = 'http://'.$_SERVER['HTTP_HOST'].'/';
        if ($response['status'] == 'success' && $request->tipo_exportacion != 'excel') {
            $urlArchivo .= 'api/prefuentefinanciamiento/download/';
        }
        $response['url'] = $urlArchivo;

        return response()->json($response);
    }

    public function downloadFile($name_file)
    {
        $modelo = new PreFuenteFinanciamiento();

        return CustomService::downloadFile($name_file, $modelo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return self::store($request, $id, PreFuenteFinanciamiento::rules($request, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fuente = PreFuenteFinanciamiento::findOrFail($id);
        if (!empty($fuente)) {
            //eliminar en cascada , delete user by id
            $fuente->delete(); //ELIMINAR, DELETE
            //arreglo con datos o mensajes de respueta, array with datas o messages to return
            $response = [
                'status' => 'success',
                'code'   => 200,
                'msg'    => trans('messages.register_deleted'),
            ];
        } else {
            $response = [
                'status' => 'error',
                'msg'    => trans('messages.register_not_found'),
            ];
        }

        return response()->json($response);
    }
}
