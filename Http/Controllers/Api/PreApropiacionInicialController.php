<?php

namespace Modules\Presupuesto\Http\Controllers\Api;

use App\Rules\Money;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Modules\Comun\Entities\ComActoAdministrativo;
use Modules\Comun\Entities\ComDocumento;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Services\ComEncabezadoDocumentoService;
use Modules\Comun\Transformers\EncabezadoDocumento\ComEncabezadoDocumentoTransformer;
use Modules\Custom\Services\ArbolService;
use Modules\Custom\Services\CustomService;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use Modules\Presupuesto\Services\PreDetallePresupuestalService;
use Modules\Presupuesto\Services\PrePlanPresupuestalService;
use Modules\Presupuesto\Services\PreSaldoPresupuestalService;
use Validator;

class PreApropiacionInicialController extends Controller
{
    private $comEncabezadoDocumentoService;
    private $prePlanPresupuestalService;
    private $preDetallePresupuestalService;
    private $preSaldoPresupuestalService;
    private $arbolService;

    public function __construct(ComEncabezadoDocumentoService $comEncabezadoDocumentoService, PrePlanPresupuestalService $prePlanPresupuestalService,
                                 PreDetallePresupuestalService $preDetallePresupuestalService, PreSaldoPresupuestalService $preSaldoPresupuestalService,
                                ArbolService $arbolService)
    {
        $this->comEncabezadoDocumentoService = $comEncabezadoDocumentoService;
        $this->prePlanPresupuestalService = $prePlanPresupuestalService;
        $this->preDetallePresupuestalService = $preDetallePresupuestalService;
        $this->preSaldoPresupuestalService = $preSaldoPresupuestalService;
        $this->arbolService = $arbolService;
    }

    /* Display a listing of the resource.
    *
    * @return Response
    */
    public function index($empresaId = null, $vigenciaId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null' || $vigenciaId == '0') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId) || $empresaId === 'null' || $empresaId == '0') {
            $empresaId = empresaActual()->id;
        }
        $apropiacionInicial = ComEncabezadoDocumento::withoutTrashed()->where('prv_empresa_id', $empresaId)->where('com_vigencia_id', $vigenciaId)->where('com_sede_id', 1)->where('com_documento_id', 1)->first();
        if (is_null($apropiacionInicial)) {
            return self::create();
        } else {
            $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal($empresaId, $vigenciaId);
            if ($apropiacionInicial->estado == config('erp.estado_documento.confirmado')) {
                $detallesApropiacion = $planPresupuestal;
            } else {
                $detallesApropiacion = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $apropiacionInicial->detalles_presupuestales);
            }
            $response = [
                'status'               => 'success',
                'code'                 => 200,
                'encabezado_documento' => ComEncabezadoDocumentoTransformer::transform($apropiacionInicial),
                'detalles_apropiacion' => $detallesApropiacion,
                'listas'               => self::create()->getData()->listas,
            ];

            return response()->json($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = JWTAuth::user();
        $documentos = ComDocumento::withoutTrashed()
            ->where('prv_maestro_documento_id', config('erp.maestros_documentos.ai.id'))
            ->orderBy('nombre')
            ->get();
        $actosAdministrativos = ComActoAdministrativo::withoutTrashed()
            ->where('com_documento_id', '1')
            ->where('com_vigencia_id', $user->vigencia_logueada->id)
            ->where('valor', '>', '0')
            ->first();
        if (is_null($actosAdministrativos)) {
            return response()->json(['status'=>'error', 'msg'=>'Debe existir un acuerdo del plan presupuestal para la vigencia']);
        }
        $response = [
            'status' => 'success',
            'code'   => 200,
            'listas' => [
                'documentos'            => $documentos,
                'actos_administrativos' => $actosAdministrativos,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createDetail($vigenciaId = null, $empresaId = null)
    {
        if (is_null($vigenciaId)) {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId)) {
            $empresaId = empresaActual()->id;
        }
        $rubros = $this->prePlanPresupuestalService->obtenerPlanPresupuestal($empresaId, $vigenciaId);
        $response = [
            'status' => 'success',
            'code'   => 200,
            'rubros' => $rubros,
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request['com_vigencia_id'] = vigenciaActual()->id;
        }
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request['com_sede_id'] = sedeActual()->id;
        }
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date|date_format:"Y-m-d"',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money(), 'min:1'],
        ];
        $buscarDuplicidad = ComEncabezadoDocumento::withoutTrashed()
            ->where('com_sede_id', $request->com_sede_id)
            ->where('com_vigencia_id', $request->com_vigencia_id)
            ->where('com_documento_id', 1)->first();
        if (!is_null($buscarDuplicidad)) {
            $response = [
                'status'               => 'error',
                'code'                 => 403,
                'encabezado_documento' => $buscarDuplicidad,
                'msg'                  => 'Ya existe una apropiacion para esta vigencia',
            ];
            $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal(empresaActual()->id, $request['com_vigencia_id']);
            $detallesApropiacion = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $buscarDuplicidad->detalles_presupuestales);
            $response['detalles_apropiacion'] = $detallesApropiacion;

            return response()->json($response);
        }
        //lanzar validaciones,launch validates to User
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            if (isset($request['rubros'])) {
                unset($request['rubros']);
            }

            try {
                //Iniciado las transaciones
                DB::beginTransaction();
                $response = $this->comEncabezadoDocumentoService->store($request->all());
                $apropiacionInicial = ComEncabezadoDocumento::findOrFail(((object) $response['encabezado_documento'])->id);
                $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal(empresaActual()->id, $request['com_vigencia_id']);
                $rubrosConMovimiento = $planPresupuestal->where('maneja_movimiento', 1);
                foreach ($rubrosConMovimiento as $rubro) {
                    $detalleApropiacion = new PreDetallePresupuestal();
                    $detalleApropiacion->com_centro_costo_id = 1;
                    $detalleApropiacion->pre_plan_presupuestal_id = $rubro->id;
                    $detalleApropiacion->descripcion = $apropiacionInicial->descripcion;
                    $detalleApropiacion->documento_referencia_id = $apropiacionInicial->id;
                    $apropiacionInicial->detalles_presupuestales()->save($detalleApropiacion);
                }
                $response['detalles_apropiacion'] = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $apropiacionInicial->detalles_presupuestales);
                DB::commit(); //confirmo todas las transaciones
                return response()->json($response);
            } catch (\Exception $exception) {
                DB::rollback(); // hago un rollback si hubo un error en alguna transacion con la base de datos
                \Log::error('PreApropiacionInicialController::Error de excepcion metodo save', [$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => $exception->getMessage(),
                ];

                return response()->json($response);
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function actualizarDetallesApropiacion($encabezadoDocumento, $datos, $rubro)
    {
        $detallePresupuestal = PreDetallePresupuestal::withoutTrashed()->where('pre_plan_presupuestal_id', $rubro['id'])->first();
        $detallePresupuestal->com_encabezado_documento_id = $encabezadoDocumento->id;
        $detallePresupuestal->com_centro_costo_id = 2;
        $detallePresupuestal->descripcion = $datos['descripcion'];
        $detallePresupuestal->valor = $rubro['valor'];
        $detallePresupuestal->documento_referencia_id = $encabezadoDocumento->id;
        for ($i = 1; $i <= 12; $i++) {
            if ($i < 10) {
                $detallePresupuestal['pac0'.$i] = $rubro['pac'][($i - 1)];
            } else {
                $detallePresupuestal['pac'.$i] = $rubro['pac'][($i - 1)];
            }
        }
        $detallePresupuestal->save();
    }

    private function validarIgualdadPacEnRubros($rubros)
    {
        foreach ($rubros as $key => $rubro) {
            $pacs = $rubro['pac'];
            $sumatoria = 0;
            foreach ($pacs as $key => $pac) {
                $sumatoria += $pac;
            }
            if (CustomService::formatearDecimales($sumatoria, 2, ',', '.') != CustomService::formatearDecimales($rubro['valor'], 2, ',', '.')) {
                return false;
            }
        }

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        if (!isset($request->com_vigencia_id) || is_null($request->com_vigencia_id) || $request->com_vigencia_id === 'null') {
            $request->com_vigencia_id = vigenciaActual()->id;
        }
        if (!isset($request->com_sede_id) || is_null($request->com_sede_id) || $request->com_sede_id === 'null') {
            $request->com_sede_id = sedeActual()->id;
        }
        $rules = [
            'com_sede_id'      => 'required|exists:com_sedes,id|integer',
            'com_vigencia_id'  => 'required|exists:com_vigencias,id|integer',
            'com_documento_id' => 'required|exists:com_documentos,id|integer',
            'descripcion'      => 'required|string',
            // 'numero_documento' => 'required|integer',
            'fecha_documento'          => 'required|date|date_format:Y-m-d',
            'vigencia_presupuestal_id' => 'required|exists:prv_listas_elementos,id|integer',
            'total'                    => ['bail', 'required', 'numeric', new Money(), 'min:1'],
            'numero_documento'         => 'required|numeric|integer',
        ];
        if ($request->estado == 1 || $request->estado == '1') { //si el estado es confirmar
            $rules['rubros'] = 'required'; //agrego la validacion para rubros
        }
        //lanzar validaciones,launch validates to User
        $msg = '';
        $validator = Validator::make($request->all(), $rules);
        if (!($validator->fails())) {
            $rubros = json_decode($request->rubros, true);
            if (!self::validarIgualdadPacEnRubros($rubros)) {
                $msg = trans('form.label.pac_not_match');
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => $msg,
                ];

                return response()->json($response);
            }
            unset($request['rubros']);

            try {
                //Iniciado las transaciones
                DB::beginTransaction();
                $response = $this->comEncabezadoDocumentoService->store($request->all(), $id);
                if ($response['status'] === 'success') {
                    $documento = ComEncabezadoDocumento::find($response['encabezado_documento']['id']);
                    if (!is_null($rubros)) {
                        if ($this->prePlanPresupuestalService->validarIgualdadRubros($rubros)) {
                            foreach ($rubros as $key => $rubro) {
                                if ($documento->estado == config('erp.estado_documento.confirmado')) {//si la apropiacion esta confirmada
                                    $this->prePlanPresupuestalService->cambiarOAsignarValorRubro($rubro['id'], $rubro['valor'], config('erp.maestros_documentos.ai.nombre'), '=');
                                    $this->prePlanPresupuestalService->cambiarOAsignarValoresPac($rubro['id'], $rubro['pac'], '=');
                                    //$this->preSaldoPresupuestalService->guardarSaldosIniciales( $rubro['id'] );
                                    //llamar al servicio para reconstruir arbol presupuestal
                                    //$arbol = $this->prePlanPresupuestalService->obtenerArbolConsulta($request->input('prv_empresa_id'),$request->input('com_vigencia_id'));
                                    //$this->arbolService->regenerarArbol( $arbol, 'pre_plan_presupuestal_id' );
                                    self::actualizarDetallesApropiacion($documento, $request->all(), $rubro);
                                }
                                if ($documento->estado == config('erp.estado_documento.preliminar')) {//si la apropiacion esta en preliminar
                                    self::actualizarDetallesApropiacion($documento, $request->all(), $rubro);
                                }
                            }
                        } else {
                            $msg = trans('form.label.value_income_and_expenses_equal_total');

                            throw new \Exception();
                        }
                    }
                    DB::commit(); //confirmo todas las transaciones
                    $planPresupuestal = $this->prePlanPresupuestalService->obtenerPlanPresupuestal(empresaActual()->id, $request['com_vigencia_id']);
                    if ($documento->estado == config('erp.estado_documento.confirmado')) {
                        $response['detalles_apropiacion'] = $planPresupuestal;
                    } else {
                        $response['detalles_apropiacion'] = $this->prePlanPresupuestalService->combinarPlanConDetalles($planPresupuestal, $documento->detalles_presupuestales);
                    }
                }

                return response()->json($response);
            } catch (\Exception $exception) {
                DB::rollback(); // hago un rollback si hubo un error en alguna transacion con la base de datos
                \Log::error('PreApropiacionInicialController::Error de excepcion metodo update', [$exception->getMessage().' linea '.$exception->getLine()]);
                $response = [
                    'status' => 'error',
                    'code'   => 403,
                    'msg'    => $exception->getMessage().' linea '.$exception->getLine(),
                ];

                return response()->json($response);
            }
        } else {
            $response = [
                'status' => 'error',
                'code'   => 403,
                'msg'    => $validator->errors(),
            ];

            return response()->json($response);
        }
    }
}
