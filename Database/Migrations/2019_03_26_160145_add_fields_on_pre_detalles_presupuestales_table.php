<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsOnPreDetallesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->unsignedInteger('pre_fuente_financiamiento_id')->nullable()->after('documento_referencia_id');
            $table->decimal('saldo', 20, 5)->nullable()->default(0.00000)->after('valor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->dropColumn([
                'pre_fuente_financiamiento_id',
                'saldo',
            ]);
        });
    }
}
