<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldDocumentoReferenciaToPreDetallesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->integer('documento_referencia_id')->unsigned()->nullable()->after('com_encabezado_documento_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->dropColumn('documento_referencia_id');
        });
    }
}
