<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsOnPrePlanesPresupuestalesIngresosGastosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_planes_presupuestales_ingresos_gastos', function (Blueprint $table) {
            $table->unsignedInteger('agno')->after('id');
            $table->unsignedInteger('mes')->after('agno');
            $table->decimal('valor', 20, 5)->default(0.00000)->after('pre_plan_presupuestal_gasto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_planes_presupuestales_ingresos_gastos', function (Blueprint $table) {
            $table->dropColumn([
                'agno',
                'mes',
                'valor',
            ]);
        });
    }
}
