<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsIzquierdaDerechaToTablePlanPresupuestal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_planes_presupuestales', function (Blueprint $table) {
            $table->integer('izquierda')->default(0)->after('pre_plan_presupuestal_id');
            $table->integer('derecha')->default(0)->after('izquierda');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_planes_presupuestales', function (Blueprint $table) {
            $table->dropColumn('izquierda');
            $table->dropColumn('derecha');
        });
    }
}
