<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrePlanPresupuestalTable extends Migration
{
    public function up()
    {
        Schema::create('pre_planes_presupuestales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prv_empresa_id')->unsigned()
                ->comment('llave foranea a empresa para enlazar el plan presupuestal a una');
            $table->integer('com_vigencia_id')->unsigned()->comment('llave foranea a vigencia para saber el año donde se encuentra el plan');
            $table->integer('prv_elemento_tipo_rubro_id')->unsigned()
                ->comment('Llave foranea un elemento de las listas para el tipo de rubro del plan');
            $table->string('codigo_rubro', 20)
                ->comment('Codigo del rubro que se asigna al momento de crear uno nuevo');
            $table->text('nombre_rubro')->comment('Nombre que recibe el rubro creado');
            $table->integer('pre_plan_presupuestal_id')->unsigned()->nullable()
                ->comment('Llave foranea asi mismo para identificar el codigo del padre');
            $table->tinyInteger('maneja_movimiento')->default('0')
                ->comment('Bandera para conocer si el rubro creado maneja movimiento o no');
            $table->decimal('plan_inicial', 20, 5)->default('0.00')
                ->comment('Valor del plan inicial');
            $table->decimal('adicciones', 20, 5)->default('0.00')
                ->comment('Valor para las adicciones que se vayan agregando al rubro');
            $table->decimal('reducciones', 20, 5)->default('0.00')
                ->comment('Valor de las reducciones al rubro');
            $table->decimal('aplazamientos', 20, 5)->default('0.00')
                ->comment('Valor de los aplazamientos que se le hagan al rubro');
            $table->decimal('creditos', 20, 5)->default('0.00')
                ->comment('Valor de los creditos realizados');
            $table->decimal('contra_creditos', 20, 5)->default('0.00')
                ->comment('Valor de los contra creditos designados');
            $table->decimal('reconocimientos', 20, 5)->default('0.00')
                ->comment('Valor de los reconocimientos al rubro');
            $table->decimal('recaudos', 20, 5)->default('0.00')
                ->comment('valor de los recaudos realizados');
            $table->decimal('disponibilidad', 20, 5)->default('0.00')
                ->comment('Valor de la disponibilidad que se le realiza');
            $table->decimal('compromiso', 20, 5)->default('0.00')
                ->comment('Valor del compromiso asignado');
            $table->decimal('obligacion', 20, 5)->default('0.00')
                ->comment('Valor de la obligacion a realizar');
            $table->decimal('pagado', 20, 5)->default('0.00')
                ->comment('Valor de lo pagado ');
            $table->decimal('pac01', 20, 5)->default('0.00')
                ->comment('el pac del mes de Enero');
            $table->decimal('pac02', 20, 5)->default('0.00')
                ->comment('el pac del mes de Febrero');
            $table->decimal('pac03', 20, 5)->default('0.00')
                ->comment('el pac del mes de Marzo');
            $table->decimal('pac04', 20, 5)->default('0.00')
                ->comment('el pac del mes de Abril');
            $table->decimal('pac05', 20, 5)->default('0.00')
                ->comment('el pac del mes de Mayo');
            $table->decimal('pac06', 20, 5)->default('0.00')
                ->comment('el pac del mes de Junio');
            $table->decimal('pac07', 20, 5)->default('0.00')
                ->comment('el pac del mes de Julio');
            $table->decimal('pac08', 20, 5)->default('0.00')
                ->comment('el pac del mes de Agosto');
            $table->decimal('pac09', 20, 5)->default('0.00')
                ->comment('el pac del mes de Septiembre');
            $table->decimal('pac10', 20, 5)->default('0.00')
                ->comment('el pac del mes de Octubre');
            $table->decimal('pac11', 20, 5)->default('0.00')
                ->comment('el pac del mes de Noviembre');
            $table->decimal('pac12', 20, 5)->default('0.00')
                ->comment('el pac del mes de Diciembre');
            $table->string('homologacion_circular_unica', 10)->nullable()
                ->comment('Campo de Homologacion para la circular Unica de los archivos de presupuesto.');
            $table->unsignedInteger('com_cgr_id')->nullable()
                ->comment('Campo para asociar el codigo CGR');
            $table->tinyInteger('activo')->default('1')
                ->comment('Campo para conocer se el plan esta activo o no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('pre_planes_presupuestales');
    }
}
