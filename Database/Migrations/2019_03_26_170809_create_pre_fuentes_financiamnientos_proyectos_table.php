<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreFuentesFinanciamnientosProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_fuentes_financiamientos_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pre_fuente_financiamiento_id');
            $table->unsignedInteger('pre_proyecto_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_fuentes_financiamientos_proyectos');
    }
}
