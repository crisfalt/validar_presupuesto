<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoOperacionOnDetallesPresupuestales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->tinyInteger('tipo_operacion')->default(0)->after('proyecto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->dropColumn('tipo_operacion');
        });
    }
}
