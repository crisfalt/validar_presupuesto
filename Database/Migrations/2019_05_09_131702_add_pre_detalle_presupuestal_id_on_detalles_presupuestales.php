<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPreDetallePresupuestalIdOnDetallesPresupuestales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->unsignedInteger('pre_detalle_presupuestal_id')->nullable()->after('pre_plan_presupuestal_id');
            $table->foreign('pre_detalle_presupuestal_id')->references('id')->on('pre_detalles_presupuestales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->dropForeign(['pre_detalle_presupuestal_id']);
            $table->dropColumn(['pre_detalle_presupuestal_id']);
        });
    }
}
