<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreFuentesFinanciamientosPlanesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_fuentes_financiamientos_planes_presupuestales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pre_fuente_financiamiento_id');
            $table->unsignedInteger('pre_plan_presupuestal_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_fuentes_financiamientos_planes_presupuestales');
    }
}
