<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldSolicitudToPrePlanesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_planes_presupuestales', function (Blueprint $table) {
            $table->decimal('solicitud', 20, 5)->default(0.00000)->after('recaudos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_planes_presupuestales', function (Blueprint $table) {
            $table->dropColumn('solicitud');
        });
    }
}
