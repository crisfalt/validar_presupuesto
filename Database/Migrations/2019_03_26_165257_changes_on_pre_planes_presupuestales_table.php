<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangesOnPrePlanesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_planes_presupuestales', function (Blueprint $table) {
//            $table->dropForeign('com_vigencia_id');
//            $table->dropColumn([
//                'com_vigencia_id',
//            ]);
            $table->unsignedInteger('agno')->nullable()->after('prv_empresa_id');
            $table->unsignedInteger('mes')->nullable()->after('agno');
            $table->integer('nivel')->default(0)->after('derecha');
            $table->tinyInteger('vigencia_futura')->default(2)->after('nivel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_planes_presupuestales', function (Blueprint $table) {
            $table->dropColumn([
                'vigencia_futura',
                'nivel',
                'agno',
                'mes',
            ]);
//            $table->unsignedInteger('com_vigencia_id')->nullable()->after('prv_empresa_id');
//            $table->foreign('com_vigencia_id')->references('id')->on('com_vigencias');
        });
    }
}
