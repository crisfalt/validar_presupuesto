<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFieldsDecimalsToPreDetallesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->decimal('valor', 20, 5)->default(0.00000)->change();
            $table->decimal('pac01', 20, 5)->default(0.00000)->change();
            $table->decimal('pac02', 20, 5)->default(0.00000)->change();
            $table->decimal('pac03', 20, 5)->default(0.00000)->change();
            $table->decimal('pac04', 20, 5)->default(0.00000)->change();
            $table->decimal('pac05', 20, 5)->default(0.00000)->change();
            $table->decimal('pac06', 20, 5)->default(0.00000)->change();
            $table->decimal('pac07', 20, 5)->default(0.00000)->change();
            $table->decimal('pac08', 20, 5)->default(0.00000)->change();
            $table->decimal('pac09', 20, 5)->default(0.00000)->change();
            $table->decimal('pac10', 20, 5)->default(0.00000)->change();
            $table->decimal('pac11', 20, 5)->default(0.00000)->change();
            $table->decimal('pac12', 20, 5)->default(0.00000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->decimal('valor')->default(0.00000)->change();
            $table->decimal('pac01')->default(0.00000)->change();
            $table->decimal('pac02')->default(0.00000)->change();
            $table->decimal('pac03')->default(0.00000)->change();
            $table->decimal('pac04')->default(0.00000)->change();
            $table->decimal('pac05')->default(0.00000)->change();
            $table->decimal('pac06')->default(0.00000)->change();
            $table->decimal('pac07')->default(0.00000)->change();
            $table->decimal('pac08')->default(0.00000)->change();
            $table->decimal('pac09')->default(0.00000)->change();
            $table->decimal('pac10')->default(0.00000)->change();
            $table->decimal('pac11')->default(0.00000)->change();
            $table->decimal('pac12')->default(0.00000)->change();
        });
    }
}
