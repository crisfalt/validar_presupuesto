<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreCierresPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_cierres_presupuestales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agno');
            $table->integer('mes');
            $table->unsignedInteger('pre_plan_presupuestal_id')->comment('Relacion al rubro el cual pertenece el cierre');
            $table->decimal('plan_inicial', 20, 5)->default('0.00')
                ->comment('Valor del plan inicial');
            $table->decimal('adicciones', 20, 5)->default('0.00')
                ->comment('Valor para las adicciones que se vayan agregando al rubro');
            $table->decimal('reducciones', 20, 5)->default('0.00')
                ->comment('Valor de las reducciones al rubro');
            $table->decimal('aplazamientos', 20, 5)->default('0.00')
                ->comment('Valor de los aplazamientos que se le hagan al rubro');
            $table->decimal('creditos', 20, 5)->default('0.00')
                ->comment('Valor de los creditos realizados');
            $table->decimal('contra_creditos', 20, 5)->default('0.00')
                ->comment('Valor de los contra creditos designados');
            $table->decimal('reconocimientos', 20, 5)->default('0.00')
                ->comment('Valor de los reconocimientos al rubro');
            $table->decimal('recaudos', 20, 5)->default('0.00')
                ->comment('valor de los recaudos realizados');
            $table->decimal('disponibilidad', 20, 5)->default('0.00')
                ->comment('Valor de la disponibilidad que se le realiza');
            $table->decimal('compromiso', 20, 5)->default('0.00')
                ->comment('Valor del compromiso asignado');
            $table->decimal('obligacion', 20, 5)->default('0.00')
                ->comment('Valor de la obligacion a realizar');
            $table->decimal('pagado', 20, 5)->default('0.00')
                ->comment('Valor de lo pagado ');
            $table->decimal('pac01', 20, 5)->default('0.00')
                ->comment('el pac del mes de Enero');
            $table->decimal('pac02', 20, 5)->default('0.00')
                ->comment('el pac del mes de Febrero');
            $table->decimal('pac03', 20, 5)->default('0.00')
                ->comment('el pac del mes de Marzo');
            $table->decimal('pac04', 20, 5)->default('0.00')
                ->comment('el pac del mes de Abril');
            $table->decimal('pac05', 20, 5)->default('0.00')
                ->comment('el pac del mes de Mayo');
            $table->decimal('pac06', 20, 5)->default('0.00')
                ->comment('el pac del mes de Junio');
            $table->decimal('pac07', 20, 5)->default('0.00')
                ->comment('el pac del mes de Julio');
            $table->decimal('pac08', 20, 5)->default('0.00')
                ->comment('el pac del mes de Agosto');
            $table->decimal('pac09', 20, 5)->default('0.00')
                ->comment('el pac del mes de Septiembre');
            $table->decimal('pac10', 20, 5)->default('0.00')
                ->comment('el pac del mes de Octubre');
            $table->decimal('pac11', 20, 5)->default('0.00')
                ->comment('el pac del mes de Noviembre');
            $table->decimal('pac12', 20, 5)->default('0.00')
                ->comment('el pac del mes de Diciembre');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_cierres_presupuestales');
    }
}
