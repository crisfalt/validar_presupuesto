<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldActivoOnPreFuentesFinanciamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_fuentes_financiamientos', function (Blueprint $table) {
            $table->tinyInteger('activo')->default(1)->after('descripcion')
                ->comment('Campo para conocer si la fuente de financiamiento esta activa o no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_fuentes_financiamientos', function (Blueprint $table) {
            $table->dropColumn(['activo']);
        });
    }
}
