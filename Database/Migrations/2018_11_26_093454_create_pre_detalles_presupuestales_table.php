<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreDetallesPresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_detalles_presupuestales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('com_encabezado_documento_id');
            $table->unsignedInteger('com_tercero_id')->nullable();
            $table->unsignedInteger('com_centro_costo_id');
            $table->unsignedInteger('pre_plan_presupuestal_id');
            $table->text('descripcion');
            $table->unsignedInteger('proyecto_id')->nullable();
            $table->decimal('valor')->default(0.00000);
            $table->decimal('pac01')->default(0.00000);
            $table->decimal('pac02')->default(0.00000);
            $table->decimal('pac03')->default(0.00000);
            $table->decimal('pac04')->default(0.00000);
            $table->decimal('pac05')->default(0.00000);
            $table->decimal('pac06')->default(0.00000);
            $table->decimal('pac07')->default(0.00000);
            $table->decimal('pac08')->default(0.00000);
            $table->decimal('pac09')->default(0.00000);
            $table->decimal('pac10')->default(0.00000);
            $table->decimal('pac11')->default(0.00000);
            $table->decimal('pac12')->default(0.00000);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_detalles_presupuestales');
    }
}
