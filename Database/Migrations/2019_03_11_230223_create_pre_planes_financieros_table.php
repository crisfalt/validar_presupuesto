<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrePlanesFinancierosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_planes_financieros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agno');
            $table->integer('mes');
            $table->unsignedInteger('com_centro_costo_id');
            $table->unsignedInteger('pre_usuario_plan_presupuestal_id');
            $table->unsignedInteger('pre_plan_presupuestal_id');
            $table->decimal('plan_inicial', 20, 5)->default('0.00')
                ->comment('Valor del plan inicial');
            $table->decimal('pac01', 20, 5)->default('0.00')
                ->comment('el pac del mes de Enero');
            $table->decimal('pac02', 20, 5)->default('0.00')
                ->comment('el pac del mes de Febrero');
            $table->decimal('pac03', 20, 5)->default('0.00')
                ->comment('el pac del mes de Marzo');
            $table->decimal('pac04', 20, 5)->default('0.00')
                ->comment('el pac del mes de Abril');
            $table->decimal('pac05', 20, 5)->default('0.00')
                ->comment('el pac del mes de Mayo');
            $table->decimal('pac06', 20, 5)->default('0.00')
                ->comment('el pac del mes de Junio');
            $table->decimal('pac07', 20, 5)->default('0.00')
                ->comment('el pac del mes de Julio');
            $table->decimal('pac08', 20, 5)->default('0.00')
                ->comment('el pac del mes de Agosto');
            $table->decimal('pac09', 20, 5)->default('0.00')
                ->comment('el pac del mes de Septiembre');
            $table->decimal('pac10', 20, 5)->default('0.00')
                ->comment('el pac del mes de Octubre');
            $table->decimal('pac11', 20, 5)->default('0.00')
                ->comment('el pac del mes de Noviembre');
            $table->decimal('pac12', 20, 5)->default('0.00')
                ->comment('el pac del mes de Diciembre');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_planes_financieros');
    }
}
