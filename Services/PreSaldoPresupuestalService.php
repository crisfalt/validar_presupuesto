<?php
/**
 * Created by PhpStorm.
 * User: crisfalt
 * Date: 1/11/18
 * Time: 07:20 PM.
 */

namespace Modules\Presupuesto\Services;

use Modules\Custom\Services\CustomService;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use Modules\Presupuesto\Entities\PreSaldoPresupuestal;

class PreSaldoPresupuestalService
{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function guardarSaldosIniciales($rubroId)
    {
        $response = [];
        if (!is_null($rubroId)) {
            try {
                $preSaldosPresupuestales = new PreSaldoPresupuestal();
                $preSaldosPresupuestales->pre_plan_presupuestal_id = $rubroId;
                $preSaldosPresupuestales->com_centro_costo_id = 3;
                $preSaldosPresupuestales->mes_id = CustomService::mapearMes();
                $preSaldosPresupuestales->save();
            } catch (\Illuminate\Database\QueryException $exception) { // error en una sentencia SQL
                \Log::error('PreSaldoPresupuestalService::Error de base de datos metodo guardarSaldosIniciales', ['error'=>$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.error_bd'),
                ];
            } catch (PDOException $exception) { // error al buscar el driver de conexion
                \Log::error('PreSaldoPresupuestalService::Error driver base de datos metodo guardarSaldosIniciales', ['error'=>$exception->getMessage()]);
                $response = [
                    'status' => 'error',
                    'msg'    => trans('messages.bd.driver_not_found'),
                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => 'El rubro no puede ser vacio',
            ];
        }

        return response()->json($response);
    }

    /**
     * @abstract metodo para realizar los cambios de valores segun el documento en un rubro especifico
     *
     * @param $pre_plan_presupuestal_id El id del plan presupuestal que identifica un rubro
     * @param $valor El valor que se va a asignar al rubro
     * @param $accion La accion del documento presupuestal ('solicitud','disponibilidad', etc)
     * @param $operacion La operacion a realizar ( suma (+) , resta (-) )
     *
     * @return Json retorna un json describiendo si el cambio o asignacion del valor al rubro se realizo sastifactoriamente
     */
    public static function cambiarOAsignarValorRubro($pre_plan_presupuestal_id, $valor, $accion, $operacion)
    {
        $rubro = PreDetallePresupuestal::withoutTrashed()->where('pre_plan_presupuestal_id', $pre_plan_presupuestal_id)->first();
        if (!$rubro->isEmpty()) {
            switch ($accion) {
                case 'apropiacion':
                    if ($operacion == '+') {
                        $rubro->plan_inicial = ($rubro->plan_inicial + $valor);
                    } //adiccion
                    if ($operacion == '-') {
                        $rubro->plan_inicial = ($rubro->plan_inicial - $valor);
                    } //reduccion
                    if ($operacion == '=') {
                        $rubro->plan_inicial = $valor;
                    } //asignar
                    $rubro->save();
                    break;
                case 'disponibilidad':
                    if ($operacion == '+') {
                        $rubro->disponibilidad = ($rubro->disponibilidad + $valor);
                    }
                    if ($operacion == '-') {
                        $rubro->disponibilidad = ($rubro->disponibilidad - $valor);
                    }
                    if ($operacion == '=') {
                        $rubro->disponibilidad = $valor;
                    } //asignar
                    $rubro->save();
                    break;
                case 'compromiso':
                    if ($operacion == '+') {
                        $rubro->compromiso = ($rubro->compromiso + $valor);
                    }
                    if ($operacion == '-') {
                        $rubro->compromiso = ($rubro->compromiso - $valor);
                    }
                    if ($operacion == '=') {
                        $rubro->compromiso = $valor;
                    } //asignar
                    $rubro->save();
                    break;
                case 'obligacion':
                    if ($operacion == '+') {
                        $rubro->obligacion = ($rubro->obligacion + $valor);
                    }
                    if ($operacion == '-') {
                        $rubro->obligacion = ($rubro->obligacion - $valor);
                    }
                    if ($operacion == '=') {
                        $rubro->obligacion = $valor;
                    } //asignar
                    $rubro->save();
                    break;
            }
            $response = [
                'status' => 'success',
                'code'   => 200,
                'msg'    => 'rubro tuvo afectaciones',
            ];
        } else {
            $response = [
                'status' => 'error',
                'msg'    => trans('messages.register_not_found'),
            ];
        }

        return response()->json($response);
    }
}
