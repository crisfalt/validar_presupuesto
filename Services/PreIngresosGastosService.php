<?php

namespace Modules\Presupuesto\Services;

use Modules\Presupuesto\Entities\PrePlanPresupuestal;

class PreIngresosGastosService
{
    /**
     * @param PrePlanPresupuestal $rubroIngreso
     */
    public static function obtenerGastosPorIngreso(PrePlanPresupuestal $rubroIngreso)
    {
        return ($rubroIngreso->rubros_gastos) ? $rubroIngreso->rubros_gastos : 'Sin rubros de gastos asignados';
    }

    public static function obtenerIngresosPorGasto(PrePlanPresupuestal $rubroGasto)
    {
        return ($rubroGasto->rubros_ingresos) ? $rubroGasto->rubros_ingresos : 'Sin rubros de ingresos asignados';
    }
}
