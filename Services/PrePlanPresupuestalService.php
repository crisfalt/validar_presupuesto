<?php
/**
 * Created by PhpStorm.
 * User: crisfalt
 * Date: 1/11/18
 * Time: 07:20 PM.
 */

namespace Modules\Presupuesto\Services;

use Exception;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Modules\Custom\Services\ArbolService;
use Modules\Presupuesto\Entities\PrePlanPresupuestal;

class PrePlanPresupuestalService
{
    private $arbolService;

    /**
     * @abstract contructor de la clase PrePlanPresupuestalService
     *
     * @param ArbolService $arbolService servicio que maneja la estructura de un arbol
     */
    public function __construct(ArbolService $arbolService)
    {
        $this->arbolService = $arbolService;
    }

    /**
     * @abstract metodo para obtener el arbol para poder consultarlo
     *
     * @param $empresaId El id de la empresa para obtener su plan presupuestal
     * @param $vigenciaId La vigencia del plan presupuestal que necesitamos obtener
     *
     * @return Arbol retorna el arbol presupuestal segun los parametros de busqueda
     */
    public function obtenerArbolConsulta($empresaId = null, $vigenciaId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId) || $empresaId === 'null') {
            $empresaId = empresaActual()->id;
        }
        $arbol = PrePlanPresupuestal::withoutTrashed()->where('com_vigencia_id', $vigenciaId)
            ->where('prv_empresa_id', $empresaId)
            ->orderBy('pre_plan_presupuestal_id', 'ASC')
            ->orderBy('id', 'ASC')
            //->get(['id','izquierda','derecha','pre_plan_presupuestal_id']);
            ->get();

        return $arbol;
    }

    /**
     * @abstract metodo para obtener el plan presupuestal odenado por padres e hijos
     *
     * @param $empresaId El id de la empresa para obtener su plan presupuestal
     * @param $vigenciaId La vigencia del plan presupuestal que necesitamos obtener
     *
     * @return Json retorna el arbol presupuestal ordenado si todo esta bien o un error en caso de existir
     */
    public function obtenerPlanPresupuestal($empresaId = null, $vigenciaId = null)
    {
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = vigenciaActual()->id;
        }
        if (is_null($empresaId) || $empresaId === 'null') {
            $empresaId = empresaActual()->id;
        }
        $arbol = self::obtenerArbolConsulta($empresaId, $vigenciaId);
        $this->arbolService->regenerarArbol($arbol, 'pre_plan_presupuestal_id');
        $planPresupuestal = $this->arbolService->obtenerData();
        //dd( $planPresupuestal );
        return $planPresupuestal;
    }

    /**
     * @abstract metodo para obtener todos los rubros de una institucio y vigencia segun el tipo de rubro
     *
     * @param $vigencia La vigencia del plan presupuestal que necesitamos obtener
     * @param $empresa El id de la empresa para obtener su plan presupuestal
     * @param $tipoRubro El tipo de rubro para saber si es de Ingreso (88) o Gasto (89)
     *
     * @return PrePlanPresupuestal retorna el plan presupuestal segun los parametros de busqueda
     */
    public function obtenerRubrosPorTipo($vigencia, $empresa, $tipoRubro)
    {
        return PrePlanPresupuestal::withoutTrashed()->planvigenciainstitucion($vigencia, $empresa)->where('tipo_rubro_id', $tipoRubro)->orderBy('codigo_rubro', 'ASC')->get();
    }

    /**
     * @abstract metodo para obtener todos los rubros de una institucio y vigencia segun el tipo de rubro
     *
     * @param $vigencia La vigencia del plan presupuestal que necesitamos obtener
     * @param $empresa El id de la empresa para obtener su plan presupuestal
     * @param $tipoRubro El tipo de rubro para saber si es de Ingreso (88) o Gasto (89)
     *
     * @return PrePlanPresupuestal retorna el plan presupuestal segun los parametros de busqueda
     */
    public function obtenerRubroPorId($id)
    {
        return PrePlanPresupuestal::find($id);
    }

    /**
     * metodo para realizar los cambios de valores segun el documento en un rubro especifico.
     *
     * @param $empresaId El id de la empresa para obtener su plan presupuestal
     * @param $vigenciaId La vigencia del plan presupuestal que necesitamos obtener
     *
     * @return Json retorna un json describiendo si el cambio o asignacion del valor al rubro se realizo sastifactoriamente
     */
    public function obtenerRubrosHijosConMovimiento($empresaId = null, $vigenciaId = null)
    {
        $user = JWTAuth::user();
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = $user->vigencia_logueada->id;
        }
        if (is_null($empresaId) || $empresaId === 'null') {
            $empresaId = $user->empresa_logueada->id;
        }
        $rubros = collect(self::obtenerPlanPresupuestal($empresaId, $vigenciaId)->where('maneja_movimiento', '1'));

        return $rubros;
    }

    /**
     * metodo para realizar los cambios de valores segun el documento en un rubro especifico.
     *
     * @param $empresaId El id de la empresa para obtener su plan presupuestal
     * @param $vigenciaId La vigencia del plan presupuestal que necesitamos obtener
     *
     * @return Json retorna un json describiendo si el cambio o asignacion del valor al rubro se realizo sastifactoriamente
     */
    public function obtenerRubrosSinMovimiento($empresaId = null, $vigenciaId = null)
    {
        $user = JWTAuth::user();
        if (is_null($vigenciaId) || $vigenciaId === 'null') {
            $vigenciaId = $user->vigencia_logueada->id;
        }
        if (is_null($empresaId) || $empresaId === 'null') {
            $empresaId = $user->empresa_logueada->id;
        }
        $rubros = collect(self::obtenerPlanPresupuestal($empresaId, $vigenciaId))->where('maneja_movimiento', '2');

        return $rubros;
    }

    /**
     * @abstract metodo para realizar los cambios de valores segun el documento en un rubro especifico
     *
     * @param $pre_plan_presupuestal_id El id del plan presupuestal que identifica un rubro
     * @param $valor El valor que se va a asignar al rubro
     * @param $accion La accion del documento presupuestal ('solicitud','disponibilidad', etc)
     * @param $operacion La operacion a realizar ( suma (+) , resta (-) )
     *
     * @return Json retorna un json describiendo si el cambio o asignacion del valor al rubro se realizo sastifactoriamente
     */
    public static function cambiarOAsignarValoresPac($pre_plan_presupuestal_id, $pacs, $operacion)
    {
        $rubro = PrePlanPresupuestal::find($pre_plan_presupuestal_id);
        if (!empty($rubro)) {
            switch ($operacion) {
                case '=':
                    for ($i = 1; $i <= 12; $i++) {
                        if ($i < 10) {
                            $rubro['pac0'.$i] = $pacs[($i - 1)];
                        } else {
                            $rubro['pac'.$i] = $pacs[($i - 1)];
                        }
                    }
                    break;
            }
            $rubro->save();
        }
    }

    /**
     * @abstract metodo para realizar los cambios de valores segun el documento en un rubro especifico
     *
     * @param $pre_plan_presupuestal_id El id del plan presupuestal que identifica un rubro
     * @param $valor El valor que se va a asignar al rubro
     * @param $accion La accion del documento presupuestal ('solicitud','disponibilidad', etc)
     * @param $operacion La operacion a realizar ( suma (+) , resta (-) )
     *
     * @return Json retorna un json describiendo si el cambio o asignacion del valor al rubro se realizo sastifactoriamente
     */
    public static function cambiarOAsignarValorRubro($pre_plan_presupuestal_id, $valor, $accion, $operacion)
    {
        $rubro = PrePlanPresupuestal::find($pre_plan_presupuestal_id);
        if (!empty($rubro)) {
            try {
                switch ($accion) {
                    case strtolower(config('erp.maestros_documentos.ai.nombre')):
                        if ($operacion == '+') {
                            $rubro->plan_inicial = ($rubro->plan_inicial + $valor);
                        } //adiccion
                        if ($operacion == '-') {
                            $rubro->plan_inicial = ($rubro->plan_inicial - $valor);
                        } //reduccion
                        if ($operacion == '=') {
                            $rubro->plan_inicial = $valor;
                        } //asignar
                        $rubro->save();
                        break;
                    case strtolower(config('erp.maestros_documentos.cdp.nombre')):
                        if ($operacion == '+') {
                            $rubro->disponibilidad = ($rubro->disponibilidad + $valor);
                        }
                        if ($operacion == '-') {
                            $rubro->disponibilidad = ($rubro->disponibilidad - $valor);
                        }
                        if ($operacion == '=') {
                            $rubro->disponibilidad = $valor;
                        } //asignar
                        break;
                    case strtolower(config('erp.maestros_documentos.com.nombre')):
                        if ($operacion == '+') {
                            $rubro->compromiso = ($rubro->compromiso + $valor);
                        }
                        if ($operacion == '-') {
                            $rubro->compromiso = ($rubro->compromiso - $valor);
                        }
                        if ($operacion == '=') {
                            $rubro->compromiso = $valor;
                        } //asignar
                        break;
                    case strtolower(config('erp.maestros_documentos.rp.nombre')):
                        if ($operacion == '+') {
                            $rubro->obligacion = ($rubro->obligacion + $valor);
                        }
                        if ($operacion == '-') {
                            $rubro->obligacion = ($rubro->obligacion - $valor);
                        }
                        if ($operacion == '=') {
                            $rubro->obligacion = $valor;
                        } //asignar
                        break;
                    case strtolower(config('erp.maestros_documentos.gp.nombre')):
                        if ($operacion == '+') {
                            $rubro->pagado = ($rubro->pagado + $valor);
                        }
                        if ($operacion == '-') {
                            $rubro->pagado = ($rubro->pagado - $valor);
                        }
                        if ($operacion == '=') {
                            $rubro->pagado = $valor;
                        } //asignar
                        break;
                    case strtolower(config('erp.maestros_documentos.r.nombre')):
                        if ($operacion == '+') {
                            $rubro->recaudos = ($rubro->recaudos + $valor);
                        }
                        if ($operacion == '-') {
                            $rubro->recaudos = ($rubro->recaudos - $valor);
                        }
                        if ($operacion == '=') {
                            $rubro->recaudos = $valor;
                        } //asignar
                        break;
                    case strtolower(config('erp.maestros_documentos.rec.nombre')):
                        if ($operacion == '+') {
                            $rubro->reconocimientos = ($rubro->reconocimientos + $valor);
                        }
                        if ($operacion == '-') {
                            $rubro->reconocimientos = ($rubro->reconocimientos - $valor);
                        }
                        if ($operacion == '=') {
                            $rubro->reconocimientos = $valor;
                        } //asignar
                        break;
                }
                $rubro->save();
                $response = [
                    'status' => 'success',
                    'code'   => 200,
                    'msg'    => 'rubro tuvo afectaciones',
                ];

                return response()->json($response);
            } catch (Exception $exception) {
                Log::error('PrePlanPresupuestalService::Error de excepcion metodo cambiarOAsignarValorRubro ', [$exception->getMessage()]);

                return response()->json(['msg' => 'PrePlanPresupuestalService::Error de excepcion metodo cambiarOAsignarValorRubro '.$exception->getMessage(), 'status' => 'error', 'code'=> 403], 403);
            }
        } else {
            $response = [
                'status' => 'error',
                'msg'    => trans('messages.register_not_found'),
            ];

            return response()->json($response);
        }
    }

    public function validarIgualdadRubros($rubros)
    {
        $sumatoriaRubrosGastos = 0;
        $sumatoriaRubrosIngresos = 0;
        foreach ($rubros as $rubro) {
            if (PrePlanPresupuestal::find($rubro['id'])->tipo_rubro_id === 88) {
                $sumatoriaRubrosIngresos += $rubro['valor'];
            }
            if (PrePlanPresupuestal::find($rubro['id'])->tipo_rubro_id === 89) {
                $sumatoriaRubrosGastos += $rubro['valor'];
            }
        }
        if ($sumatoriaRubrosIngresos === $sumatoriaRubrosGastos) {
            return true;
        }

        return false;
    }

    // combina los detalles presupuestales con los rubros de los planes donde coinciden
    public function combinarPlanConDetalles($padres, $detalles)
    {
        $union = collect();
        foreach ($padres as $padre) {
            if ($padre['maneja_movimiento'] == config('erp.mapeo_si_no.SI')) {
                foreach ($detalles as $detalle) {
                    if ((int) $padre['codigo_rubro'] === (int) $detalle['plan_presupuestal']['codigo_rubro']) {
                        if ($detalle['valor'] > 0) {
                            $mix = $detalle['plan_presupuestal'];
                            $mix['plan_inicial'] = $detalle['valor'];
                            for ($k = 1; $k <= 12; $k++) {
                                ($k <= 9) ? $mix['pac0'.$k] = $detalle['pac0'.$k] : $mix['pac'.$k] = $detalle['pac'.$k];
                            }
                            $union->push($mix);
                            break;
                        } else {
                            $union->push($padre);
                            break;
                        }
                    }
                }
            } else {
                $union->push($padre);
            }
        }

        return $union;
    }

    public static function saldoDisponible(Model $rubro)
    {
        return  $rubro->plan_inicial - $rubro->disponibilidad + $rubro->adicciones - $rubro->reducciones + $rubro->creditos - $rubro->contra_creditos - $rubro->aplazamientos;
    }

    public function fuentesPorRubro($rubro)
    {
        return ($rubro->fuentes_financiamientos) ? $rubro->fuentes_financiamientos : null;
    }
}
