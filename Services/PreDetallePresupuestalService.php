<?php

namespace Modules\Presupuesto\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Modules\Presupuesto\Entities\PreDetallePresupuestal;
use PDOException;

/**
 * Class PreDetallePresupuestalService.
 */
class PreDetallePresupuestalService
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function obtenerDetalles($comEncabezadoDocumentoId)
    {
        $detallesPresupuestales = PreDetallePresupuestal::withoutTrashed()->where('com_encabezado_documento_id', $comEncabezadoDocumentoId)->get();

        $codigoDocumento = codigoDocumento($comEncabezadoDocumentoId);

        if ($codigoDocumento == config('erp.maestros_documentos.api.prefijo') || $codigoDocumento == config('erp.maestros_documentos.rpi.prefijo')) {
            $detallesPresupuestales = $detallesPresupuestales->filter(function ($detalle) {
                return $detalle->plan_presupuestal->tipo_rubro_id == config('erp.rubros.ingresos');
            });
        }

        if ($codigoDocumento == config('erp.maestros_documentos.ccg.prefijo')) {
            $detallesPresupuestales = $detallesPresupuestales->where('pre_detalle_presupuestal_id', '=', null);
        }

        return $detallesPresupuestales;
    }

    /**
     * @param Model $encabezadoOrigen
     * @param Model $encabezadoDestino
     *
     * @return bool
     */
    public function copiarDetalles(Model $encabezadoOrigen, Model $encabezadoDestino)
    {
        $detallesOrigen = $this->obtenerDetalles($encabezadoOrigen['id']);
        foreach ($detallesOrigen as $detalleOrigen) {
            try {
                $detalleDestino = new PreDetallePresupuestal();
                $detalleDestino->com_encabezado_documento_id = $encabezadoDestino['id'];
                $detalleDestino->com_tercero_id = $detalleOrigen['com_tercero_id'];
                $detalleDestino->com_centro_costo_id = $detalleOrigen['com_centro_costo_id'];
                $detalleDestino->pre_plan_presupuestal_id = $detalleOrigen['pre_plan_presupuestal_id'];
                $detalleDestino->descripcion = $detalleOrigen['descripcion'];
                $detalleDestino->proyecto_id = $detalleOrigen['proyecto_id'];
                $detalleDestino->valor = $detalleOrigen['valor'];
                $detalleDestino->pac01 = $detalleOrigen['pac01'];
                $detalleDestino->pac01 = $detalleOrigen['pac01'];
                $detalleDestino->pac03 = $detalleOrigen['pac03'];
                $detalleDestino->pac04 = $detalleOrigen['pac04'];
                $detalleDestino->pac05 = $detalleOrigen['pac05'];
                $detalleDestino->pac06 = $detalleOrigen['pac06'];
                $detalleDestino->pac07 = $detalleOrigen['pac07'];
                $detalleDestino->pac08 = $detalleOrigen['pac08'];
                $detalleDestino->pac09 = $detalleOrigen['pac09'];
                $detalleDestino->pac10 = $detalleOrigen['pac10'];
                $detalleDestino->pac11 = $detalleOrigen['pac11'];
                $detalleDestino->pac12 = $detalleOrigen['pac12'];
                $detalleDestino->documento_referencia_id = $encabezadoDestino['documento_referencia_id'];
                $detalleDestino->save();
            } catch (QueryException $exception) { // error en una sentencia SQL
                \Log::error('PreDetallePresupuestalService::Error de base de datos metodo copiarDetalles', ['error'=>$exception->getMessage()]);

                return false;
            } catch (PDOException $exception) { // error al buscar el driver de conexion
                \Log::error('PreDetallePresupuestalService::Error driver base de datos metodo copiarDetalles', ['error'=>$exception->getMessage()]);

                return false;
            }
        }

        return true;
    }
}
