<?php

namespace Modules\Presupuesto\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class PreSolicitudPresupuestal extends Model
{
    use Notifiable;

    protected $table = 'com_encabezado_documento';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    //protected $hidden = ['created_at','updated_at','deleted_at'];

    public static $exportColumns = ['estado', 'com_vigencia_id', 'com_documento_id', 'numero_documento', 'fecha_documento', 'total'];
    private $nameFileExport = 'Solicitudes presupuestales';
    public static $defaultOrder = ['created_at'];
    public static $directionOrder = ['DESC'];

    public function getNameFileExportAttribute()
    {
        return $this->nameFileExport;
    }
}
