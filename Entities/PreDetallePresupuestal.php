<?php

namespace Modules\Presupuesto\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Comun\Entities\ComCentroCosto;
use Modules\Comun\Entities\ComEncabezadoDocumento;
use Modules\Comun\Entities\ComTercero;

class PreDetallePresupuestal extends Model
{
    use Notifiable;

    protected $table = 'pre_detalles_presupuestales';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
//    protected $appends = ['codigo_con_numero_documento'];

    public static $rules = [
        'com_encabezado_documento_id' => 'required|exists:com_encabezados_documentos,id',
        'com_tercero_id'              => 'nullable',
        'proyecto_id'                 => 'nullable',
        'com_centro_costo_id'         => 'nullable|exists:com_centros_costos,id',
        'pre_plan_presupuestal_id'    => 'required|exists:pre_planes_presupuestales,id',
        'descripcion'                 => 'nullable|min:10',
        'valor'                       => 'required|max:20|regex:/^\d*(\.\d{1,5})?$/',
        'pre_detalle_presupuestal_id' => 'nullable',
    ];

    public function encabezado_documento()
    {
        return $this->belongsTo(ComEncabezadoDocumento::class, 'com_encabezado_documento_id', 'id');
    }

    public function encabezado_documento_referencia()
    {
        return $this->belongsTo(ComEncabezadoDocumento::class, 'documento_referencia_id', 'id');
    }

    public function getCodigoConNumeroDocumentoAttribute()
    {
        $documentoReferencia = $this->encabezado_documento_referencia;
        if (!is_null($documentoReferencia)) {
            return $this->encabezado_documento_referencia->documento->codigo.'-'.$this->encabezado_documento_referencia->numero_documento;
        } else {
            null;
        }
    }

    public function tercero()
    {
        return $this->belongsTo(ComTercero::class, 'com_tercero_id', 'id');
    }

    public function centro_costo()
    {
        return $this->belongsTo(ComCentroCosto::class, 'com_centro_costo_id', 'id');
    }

    public function plan_presupuestal()
    {
        return $this->belongsTo(PrePlanPresupuestal::class, 'pre_plan_presupuestal_id', 'id');
    }

    public function proyecto()
    {
        return $this->belongsTo(PreProyecto::class, 'proyecto_id', 'id');
    }

    public function detalles_hijos()
    {
        return $this->hasMany(self::class);
    }

    public function detalles_padre()
    {
        return $this->belongsTo(self::class);
    }

    public function fuente_financiamiento()
    {
        return $this->belongsTo(PreFuenteFinanciamiento::class, 'pre_fuente_financiamiento_id', 'id');
    }

    public function scopeRubroEncabezado($query, $rubro, $comDocumentoId)
    {
        return $query->where('prv_empresa_id', '=', $empresa)->where('com_vigencia_id', $vigencia);
    }
}
