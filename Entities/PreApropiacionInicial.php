<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 15/02/2019
 * Time: 2:45 PM.
 */

namespace Modules\Presupuesto\Entities;

use Modules\Comun\Entities\ComEncabezadoDocumento;

class PreApropiacionInicial extends ComEncabezadoDocumento
{
    protected $table = 'com_encabezados_documentos';

    public static $exportColumns = ['estado', 'com_vigencia_id', 'com_documento_id', 'numero_documento', 'fecha_documento', 'total'];
    private $nameFileExport = 'Apropiacion inicial';
    public static $defaultOrder = ['created_at'];
    public static $directionOrder = ['DESC'];

    public function getNameFileExportAttribute()
    {
        return $this->nameFileExport;
    }
}
