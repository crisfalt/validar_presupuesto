<?php

namespace Modules\Presupuesto\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\Rule;
use Modules\Comun\Entities\ComCgr;
use Modules\Comun\Entities\ComVigencia;
use Modules\Privado\Entities\PrvEmpresa;
use Modules\User\Entities\Sentinel\User;
use Sofa\Eloquence\Eloquence;

/**
 * Class PrePlanPresupuestal.
 */
class PrePlanPresupuestal extends Model
{
    use Notifiable, SoftDeletes, Eloquence;

    /**
     * @var string
     */
    protected $table = 'pre_planes_presupuestales';
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['institucion_id', 'codigo_rubro'];
    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    //protected $appends = ['codigo_con_nombre'];
    /**
     * @var array
     */
    public static $exportColumns = ['com_vigencia_id', 'codigo_rubro', 'nombre_rubro', 'maneja_movimiento', 'activo'];
    /**
     * @var string
     */
    private $nameFileExport = 'Plan presupuestal';

    // define default searchable columns:
    /**
     * @var array
     */
    protected $searchableColumns = [
        'codigo_rubro',
        'nombre_rubro',
    ];

    /**
     * @var array
     */
    public static $resultColumns = ['id', 'codigo_rubro', 'nombre_rubro', 'maneja_movimiento', 'plan_inicial'];

    /**
     * @var array
     */
    public static $defaultOrder = ['codigo_rubro'];
    /**
     * @var array
     */
    public static $directionOrder = ['ASC'];

    /**
     * @param Request $request
     * @param null    $id
     *
     * @return array
     */
    public static function rules(Request $request, $id = null)
    {
        $rules = [
            'nombre_rubro'      => 'required|string',
            'prv_empresa_id'    => 'required',
            'com_vigencia_id'   => 'required',
            'tipo_rubro_id'     => 'required',
            'maneja_movimiento' => 'in:1,2',
            'activo'            => 'in:1,2',
        ];
        switch ($request->method()) {
            case 'POST':
            {
                $validacionCodigo = ['codigo_rubro' => [
                        'required',
                        'string',
                        'max:20',
                        Rule::unique('pre_planes_presupuestales', 'codigo_rubro')->where(function ($query) use ($request) {
                            return $query->where('prv_empresa_id', $request->prv_empresa_id)
                                        ->where('com_vigencia_id', $request->input('com_vigencia_id'));
                        }),
                    ],
                ];

                return array_merge($rules, $validacionCodigo);
            }
            case 'PUT':
            {
                $validacionCodigo = ['codigo_rubro' => [
                        'required',
                        'string',
                        'max:20',
                        Rule::unique('pre_planes_presupuestales', 'codigo_rubro')
                            ->ignore($id, 'id')
                            ->where(function ($query) use ($request) {
                                return $query->where('prv_empresa_id', $request->prv_empresa_id)
                                    ->where('com_vigencia_id', $request->input('com_vigencia_id'));
                            }),
                    ],
                ];

                return array_merge($rules, $validacionCodigo);
            }
            default:break;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vigencia()
    {
        return $this->belongsTo(ComVigencia::class, 'com_vigencia_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codigo_cgr()
    {
        return $this->belongsTo(ComCgr::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo(PrvEmpresa::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function padre()
    {
        return $this->belongsTo(self::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hijos()
    {
        return $this->hasMany(self::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fuentes_financiamientos()
    {
        return $this->belongsToMany(PreFuenteFinanciamiento::class, 'pre_fuentes_financiamientos_planes_presupuestales', 'pre_plan_presupuestal_id', 'pre_fuente_financiamiento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'pre_usuarios_planes_presupuestales', 'pre_plan_presupuestal_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rubros_ingresos()
    {
        return $this->belongsToMany(self::class, 'pre_planes_presupuestales_ingresos_gastos', 'pre_plan_presupuestal_gasto_id', 'pre_plan_presupuestal_ingreso_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rubros_gastos()
    {
        return $this->belongsToMany(self::class, 'pre_planes_presupuestales_ingresos_gastos', 'pre_plan_presupuestal_ingreso_id', 'pre_plan_presupuestal_gasto_id');
    }

    public function detalles_presupuestales()
    {
        return $this->hasMany(PreDetallePresupuestal::class);
    }

    public function detalleRubro($comEncabezadoDocumentoId)
    {
        return $this->detalles_presupuestales()->where('com_encabezado_documento_id', '=', $comEncabezadoDocumentoId)->first();
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePlanVigenciaInstitucion($query, $vigencia, $empresa)
    {
        return $query->where('prv_empresa_id', '=', $empresa)->where('com_vigencia_id', $vigencia);
    }

    /**
     * @return string
     */
    public function getCodigoConNombreAttribute()
    {
        return $this->codigo_rubro.' - '.$this->nombre_rubro;
    }

    /**
     * @return string
     */
    public function getNameFileExportAttribute()
    {
        return $this->nameFileExport;
    }
}
