<?php
/**
 * Created by PhpStorm.
 * User: CRISFALT
 * Date: 29/3/2019
 * Time: 14:39.
 */

namespace Modules\Presupuesto\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class PreProyecto extends Model
{
    use Notifiable, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'pre_proyectos';
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
