<?php
/**
 * Created by PhpStorm.
 * User: crisf
 * Date: 13/03/2019
 * Time: 10:38 PM.
 */

namespace Modules\Presupuesto\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;

class PreFuenteFinanciamiento extends Model
{
    use Notifiable, SoftDeletes;

    protected $table = 'pre_fuentes_financiamientos';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    public static $exportColumns = ['codigo', 'nombre', 'descripcion', 'activo'];
    private $nameFileExport = 'Fuentes de Financiamiento'; // nombre para poner al archivo al momento de exportar la grilla
    public static $defaultOrder = ['nombre'];
    public static $directionOrder = ['ASC'];

    public static function rules(Request $request, $id = null)
    {
        switch ($request->method()) {
            case 'POST':
                {
                    if (is_null($id)) {
                        return [
                            'codigo'      => 'required|string|unique:pre_fuentes_financiamientos,codigo',
                            'nombre'      => 'required|string|max:250',
                            'descripcion' => 'required|string',
                            //'rubros' => 'json',
                            'activo' => 'in:1,2',
                        ];
                    } else {
                        return [
                            'codigo'      => 'required|string|unique:pre_fuentes_financiamientos,codigo'.','.$id,
                            'nombre'      => 'required|string|max:250',
                            'descripcion' => 'required|string',
                            //'rubros' => 'json',
                            'activo' => 'in:1,2',
                        ];
                    }
                }
            case 'PUT':
                {
                    return [
                        'codigo'      => 'required|string|unique:pre_fuentes_financiamientos,codigo'.','.$id,
                        'nombre'      => 'required|string|max:250',
                        'descripcion' => 'required|string',
                        //'rubros' => 'json',
                        'activo' => 'in:1,2',
                    ];
                }
            default:
                break;
        }
    }

    public function rubros()
    {
        return $this->belongsToMany(PrePlanPresupuestal::class, 'pre_fuentes_financiamientos_planes_presupuestales', 'pre_fuente_financiamiento_id', 'pre_plan_presupuestal_id');
    }

    public function getNameFileExportAttribute()
    {
        return $this->nameFileExport;
    }
}
